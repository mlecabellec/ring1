if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
    })(jQuery);
}


function updateGraphWithBothSearches()
{
    var rightString = $("#rightSearchBox").val();
    var leftString = $("#leftSearchBox").val();

    if ((leftString.length) <= 2 && (rightString.length > 2))
    {
        leftString = rightString;
    }
    if ((leftString.length) <= 2 && (rightString.length > 2))
    {
        leftString = rightString;
    }

    if ((leftString.length) > 2 && (rightString.length > 2))
    {
        leftString = "*" + leftString + "*";
        rightString = "*" + rightString + "*";

        $.ajax({
            url: "/ring1/networkNavigator/get_json_graph_from_double_request",
            data: {leftString: leftString, rightString: rightString},
            dataType: "json"})
                .done(function(data) {//If OK



                })
                .fail(function(data) {
                    alert("error: " + data);
                });
    }

}


function leftSearchHandler(event)
{
    var searchString = $(event.target).val();

    if (searchString.length > 2)
    {
        $.post("/ring1/networkNavigator/search_related_items", {q: searchString, side: "left"})
                .done(function(data) {//If OK

                    //alert(data);

                    //replace with new view of the item
                    $("#leftSearchResults").empty().prepend(data);


                })
                .fail(function(data) {
                    alert("error: " + data);
                });
    }
}

function rightSearchHandler(event)
{
    var searchString = $(event.target).val();

    if (searchString.length > 2)
    {
        $.post("/ring1/networkNavigator/search_related_items", {q: searchString, side: "right"})
                .done(function(data) {//If OK

                    //alert(data);

                    //replace with new view of the item
                    $("#rightSearchResults").empty().prepend(data);


                })
                .fail(function(data) {
                    alert("error: " + data);
                });
    }
}

function demoInit()
{
    var width = $("#firstWellAtTheCenter").width() - 10;
    height = 600;

    var color = d3.scale.category20();

    var force = d3.layout.force()
            .linkDistance(45)
            .linkStrength(3)
            .charge(-800)
            .chargeDistance(100)
            .theta(0.4)
            .size([width, height]);

    var svg = d3.select(document.getElementById("firstWellAtTheCenter")).append("svg")
            .attr("width", width)
            .attr("height", height);

    d3.json("/ring1/static/js/miserables.json", function(error, graph) {
        var nodes = graph.nodes.slice(),
                links = [],
                bilinks = [];

        graph.links.forEach(function(link) {
            var s = nodes[link.source],
                    t = nodes[link.target],
                    i = {}; // intermediate node
            nodes.push(i);
            links.push({source: s, target: i}, {source: i, target: t});
            bilinks.push([s, i, t]);
        });

        force.nodes(nodes)
                .links(links)
                .start();

        var link = svg.selectAll(".link")
                .data(bilinks)
                .enter().append("path")
                .attr("class", "link");

        var node = svg.selectAll(".node")
                .data(graph.nodes)
                .enter().append("circle")
                .attr("class", "node")
                .attr("r", 15)
                .style("fill", function(d) {
                    return color(d.group);
                })
                .call(force.drag);

        node.append("title")
                .text(function(d) {
                    return d.name;
                });

        force.on("tick", function() {
            link.attr("d", function(d) {
                return "M" + d[0].x + "," + d[0].y
                        + "S" + d[1].x + "," + d[1].y
                        + " " + d[2].x + "," + d[2].y;
            });
            node.attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
        });
    });


    $.ajax({
        url: "/ring1/networkNavigator/get_json_graph_from_double_request",
        data: {leftString: "*vlad*", rightString: "*boot*"},
        dataType: "json"})
            .done(function(data) {//If OK


            })
            .fail(function(data) {
                alert("error: " + data);
            });




}


var network_d3_forceLayout;
var network_d3_svgContext;
var network_d3_colorUtility;
var network_d3_svgLinks;
var network_d3_svgNodes;
var network_d3_svgTexts;

function initNetworkNavigator()
{

    //wire events for search (left)
    $("#leftSearchBox").bind("input change blur keydown beforeinput", leftSearchHandler);
    $("#leftSearchButton").bind("click", leftSearchHandler);

    //wire events for search (right)
    $("#rightSearchBox").bind("input change blur keydown beforeinput", rightSearchHandler);
    $("#rightSearchButton").bind("click", rightSearchHandler);


    var width = $("#firstWellAtTheCenter").width() - 10;
    height = 600;


    network_d3_colorUtility = d3.scale.category20();

    network_d3_forceLayout = d3.layout.force()
            .linkDistance(130)
            .charge(-800)
            .chargeDistance(500)
            .theta(0.4)
            .size([width, height]);

    network_d3_svgContext = d3.select(document.getElementById("firstWellAtTheCenter")).append("svg")
            .attr("width", width)
            .attr("height", height);


    $.ajax({
        url: "/ring1/networkNavigator/get_json_graph_selfcentered",
        dataType: "json"})
            .done(function(data) {//If OK

                network_d3_forceLayout
                        .nodes(data.nodes)
                        .links(data.links)
                        .start();

                network_d3_svgLinks = network_d3_svgContext.selectAll(".link")
                        .data(data.links)
                        .enter().append("line")
                        .attr("class", "link")
                        .style("stroke-width", function(d) {
                            return Math.sqrt(d.value);
                        });

                network_d3_svgNodes = network_d3_svgContext.selectAll(".node")
                        .data(data.nodes)
                        .enter().append("circle")
                        .attr("class", function(d) {
                            return "node " + d.node_class;
                        })
                        .attr("r", 15)
                        .style("fill", "#8282b4")
                        .style("fill-opacity", "1.0")
                        .style("stroke-width", "0.27")
                        .style("stroke-opacity", "0.5")
                        .style("stroke", "#000000")
                        .call(network_d3_forceLayout.drag);

                network_d3_svgTexts = network_d3_svgContext.selectAll(".text")
                        .data(data.nodes)
                        .enter().append("text")
                        .attr("class", "text")
                        .style("text-align", "center")
                        .style("text-anchor", "middle")
                        .style("font-size", "15")
                        .style("fill", "#000000")
                        .style("fill-opacity", "0.8")
                        .style("sroke", "none")
                        .text(function(d) {
                            if (d.node_class == "Person")
                            {
                                return d.node_data.fullName;
                            }else if (d.node_class == "Tag")
                            {
                                return d.node_data.defaultLabel;
                            }else if (d.node_class == "Task")
                            {
                                return d.node_data.shortDescription;
                            }else if (d.node_class == "Activity")
                            {
                                return d.node_data.shortDescription;
                            }else
                            {
                                return d.node_text;
                            }
                        });


                network_d3_forceLayout.on("tick", function() {
                    network_d3_svgLinks.attr("x1", function(d) {
                        return d.source.x;
                    })
                            .attr("y1", function(d) {
                                return d.source.y;
                            })
                            .attr("x2", function(d) {
                                return d.target.x;
                            })
                            .attr("y2", function(d) {
                                return d.target.y;
                            });

                    network_d3_svgNodes.attr("cx", function(d) {
                        return d.x;
                    })
                            .attr("cy", function(d) {
                                return d.y;
                            });

                    network_d3_svgTexts.attr("x", function(d) {
                        return d.x;
                    })
                            .attr("y", function(d) {
                                return d.y;
                            });

                });

            })
            .fail(function(data) {
                alert("error: " + data);
            });


    //demoInit();
}


$(window).ready(function() {
    initNetworkNavigator();
});




