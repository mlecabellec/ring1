if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
    })(jQuery);
}

//CKeditor kept on hand
var longDescription_ckeditor;

//element selected by doubleclick
var selected_searchResult;


function leftSearchHandler(event)
{
    var searchString = $(event.target).val();

    if (searchString.length > 2)
    {
        $.post("/ring1/timeline/search_related_items", {q: searchString, side: "left"})
                .done(function(data) {//If OK

                    //alert(data);

                    //replace with new view of the item
                    $("#leftSearchResults").empty().prepend(data);

                    //Make all items droppable
                    $(".searchresult-item").draggable();

                    //Make all result items selectable with a double click
                    $(".searchresult-item").dblclick(searchResultDoubleClickHandler);

                })
                .fail(function(data) {
                    alert("error: " + data);
                });
    }
}

function rightSearchHandler(event)
{
    var searchString = $(event.target).val();

    if (searchString.length > 2)
    {
        $.post("/ring1/timeline/search_related_items", {q: searchString, side: "right"})
                .done(function(data) {//If OK

                    //alert(data);

                    //replace with new view of the item
                    $("#rightSearchResults").empty().prepend(data);

                    //Make all items droppable
                    $(".searchresult-item").draggable();

                    //Make all result items selectable with a double click
                    $(".searchresult-item").dblclick(searchResultDoubleClickHandler);

                })
                .fail(function(data) {
                    alert("error: " + data);
                });
    }
}


function dropOnTimelineItemHandler(event, ui) {
    var targetActivity = $(this);
    var targetActivity_id = $(this).attr("id");
    var droppedObject_id = ui.draggable.attr("id");
    ui.helper.hide("explode", {pieces: 16});
    ui.draggable.show();
    ui.helper.show();
    //alert("TODO: put some code !!! dropped = " + droppedObject_id + " on " + targetActivity_id);

    $.post("/ring1/timeline/link_to_activity", {activity_id: targetActivity_id, linked_id: droppedObject_id})
            .done(function(data) {//If OK

                //alert(data);

                //replace with new view of the item
                targetActivity.replaceWith(data);

                //Make all items droppable
                $(".timeline-item").droppable({
                    drop: dropOnTimelineItemHandler, tolerance: "pointer", accept: ".searchresult-item"});

                //Make all timelineitems linkable with a double click
                $(".timeline-item").dblclick(timelineItemClickHandler);

            })
            .fail(function(data) {
                alert("error: " + data);
            });

}

function searchResultDoubleClickHandler(event)
{
    var target = event.target;

    //It's pehaps the grand-father ?
    if ($(target).parent().parent().hasClass("searchresult-item"))
    {
        target = $(target).parent();
    }

    //Or the father ?
    if ($(target).parent().hasClass("searchresult-item"))
    {
        target = $(target).parent();
    }


    if ($(target).hasClass("searchresult-item") && !$(target).hasClass("selected-searchresult-item"))
    {//Selectable item, go on
        $("*").removeClass("selected-searchresult-item"); //remove other selection marks
        $(target).addClass("selected-searchresult-item"); //setting the selection mark
        selected_searchResult = $(target); //keep the target on hand
    } else if ($(target).hasClass("searchresult-item") && $(target).hasClass("selected-searchresult-item"))
    {//Selected, so unselect it
        $(target).removeClass("selected-searchresult-item");
        selected_searchResult = null;
    }
}

function timelineItemClickHandler(event)
{
    var target = event.target;

    //It's pehaps the grand ancestor ?
    if ($(target).parent().parent().parent().hasClass("timeline-item"))
    {
        target = $(target).parent();
    }

    //It's pehaps the grand-father ?
    if ($(target).parent().parent().hasClass("timeline-item"))
    {
        target = $(target).parent();
    }

    //Or the father ?
    if ($(target).parent().hasClass("timeline-item"))
    {
        target = $(target).parent();
    }

    //have we a timeline item and a search result?
    if ($(target).hasClass("timeline-item") && (selected_searchResult != null))
    {

        var targetActivity = $(target);
        var targetActivity_id = $(this).attr("id");
        var droppedObject_id = $(selected_searchResult).attr("id");

        $.post("/ring1/timeline/link_to_activity", {activity_id: targetActivity_id, linked_id: droppedObject_id})
                .done(function(data) {//If OK

                    //alert(data);

                    //replace with new view of the item
                    targetActivity.replaceWith(data);

                    //Make all items droppable
                    $(".timeline-item").droppable({
                        drop: dropOnTimelineItemHandler, tolerance: "pointer", accept: ".searchresult-item"});

                })
                .fail(function(data) {
                    alert("error: " + data);
                });

    }



}


function initTimeline()
{
    longDescription_ckeditor =
            CKEDITOR.replace('longDescription', {
                height: '110px',
                width: '98.565%',
                toolbar: 'Basic'}
            );
    //on load, we load the list of activities
    $("#timelineList").load("/ring1/timeline/get_my_activity_timeline");

    //Make all result items draggable
    $(".searchresult-item").draggable();

    //Make all result items selectable with a double click
    $(".searchresult-item").dblclick(searchResultDoubleClickHandler);

    //Make all items droppable
    $(".timeline-item").droppable({
        drop: dropOnTimelineItemHandler, tolerance: "pointer", accept: ".searchresult-item"});

    //Make all timelineitems linkable with a double click
    $(".timeline-item").dblclick(timelineItemClickHandler);

    //wire the submit button
    $("#submitNewActivityButton").click(function() {

        //Ensure that CK editor updated the textarea
        longDescription_ckeditor.updateElement();
        //get the data from the form
        var newActivityFormData = $("#newActivityForm").serializeArray();
        //Sending data
        $.post("/ring1/timeline/create_or_update_new_activity", newActivityFormData)
                .done(function(data) {//If OK
                    //put the new activity at the top of the list
                    $("#timelineList").prepend(data);
                    //Make all items droppable
                    $(".timeline-item").droppable({
                        drop: dropOnTimelineItemHandler, tolerance: "pointer", accept: ".searchresult-item"});

                    //Make all timelineitems linkable with a double click
                    $(".timeline-item").dblclick(timelineItemClickHandler);

                    //purge the form after creation
                    $("#shortDescription").val("");
                    $("#longDescription").val("");
                    $("#workHours").val("");
                    longDescription_ckeditor.document.getBody().setText("");

                })
                .fail(function(data) {
                    //alert("error: " + data);

                    $("#submitNewActivityButton").popover("destroy");
                    $("#submitNewActivityButton").popover({content: data.responseText, placement: "right"});
                    $("#submitNewActivityButton").popover("show");
                });
    });

    //wire events for search (left)
    $("#leftSearchBox").bind("input change blur keydown beforeinput", leftSearchHandler);
    $("#leftSearchButton").bind("click", leftSearchHandler);

    //wire events for search (right)
    $("#rightSearchBox").bind("input change blur keydown beforeinput", rightSearchHandler);
    $("#rightSearchButton").bind("click", rightSearchHandler);

}


$(window).ready(function() {
    initTimeline();
});




