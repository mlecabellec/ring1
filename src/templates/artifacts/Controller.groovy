@artifact.package@class @artifact.name@ {

    
    import static org.springframework.http.HttpStatus.*
    import grails.transaction.Transactional

    import grails.plugin.springsecurity.annotation.Secured

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index() { }
}
