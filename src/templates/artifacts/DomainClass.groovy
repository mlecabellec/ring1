@artifact.package@class @artifact.name@ {

    static mapWith = "mongo"
    static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static constraints = {
        dateCreated(nullable:true)
        lastUpdated (nullable:true)       
    }
    
    Date dateCreated
    Date lastUpdated  
    
    public String toString()
    {
        return "@artifact.name@[" + id + "]"
        //return "@artifact.name@[" + id + "], " + this.properties
    }
}
