package com.booleanworks.ring1.v1

import grails.transaction.Transactional

import grails.converters.JSON

@Transactional
class MongoSyncUtilityService {

    def mongo
    def grailsApplication
    
    def pushLastActivities() {
        
        //System.out.println("grailsApplication.config.grails.mongo.databaseName = " + grailsApplication.config.grails.mongo.databaseName)
        
        def db = mongo.getDB(grailsApplication.config.grails.mongo.databaseName)
        db.test.insert([data:"test"])
        
        int maxBurstSize = 500
        
        Activity.withTransaction(){ status ->
            Activity.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ 
                cActivity -> 
                
                //JSON converter = new JSON(cActivity) 
                //String dataString = converter.toString(false)
                //System.out.println("dataString = " + dataString)
                //System.out.println("dataString.length() = " + dataString.length())
                
                
                if(db.activity.count(variableUniversalUniqueId:cActivity.variableUniversalUniqueId) == 0)
                {
                    db.activity.insert([
                            shortDescription:cActivity.shortDescription,
                            longDescription:cActivity.longDescription,
                            owner:cActivity.owner.variableUniversalUniqueId,     
                            realWorkHours:cActivity.realWorkHours,   
                            dateCreated:cActivity.dateCreated,
                            lastUpdated:cActivity.lastUpdated,
                            invariantUniversalUniqueId:cActivity.invariantUniversalUniqueId,
                            variableUniversalUniqueId:cActivity.variableUniversalUniqueId,
                        ])                    
                }
 
            }
        }

    }
}
