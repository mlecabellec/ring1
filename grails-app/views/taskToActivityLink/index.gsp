
<%@ page import="com.booleanworks.ring1.v1.TaskToActivityLink" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'taskToActivityLink.label', default: 'TaskToActivityLink')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-taskToActivityLink" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-taskToActivityLink" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'taskToActivityLink.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'taskToActivityLink.lastUpdated.label', default: 'Last Updated')}" />
					
						<th><g:message code="taskToActivityLink.activity.label" default="Activity" /></th>
					
						<th><g:message code="taskToActivityLink.task.label" default="Task" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${taskToActivityLinkInstanceList}" status="i" var="taskToActivityLinkInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${taskToActivityLinkInstance.id}">${fieldValue(bean: taskToActivityLinkInstance, field: "dateCreated")}</g:link></td>
					
						<td><g:formatDate date="${taskToActivityLinkInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: taskToActivityLinkInstance, field: "activity")}</td>
					
						<td>${fieldValue(bean: taskToActivityLinkInstance, field: "task")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${taskToActivityLinkInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
