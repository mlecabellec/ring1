<%@ page import="com.booleanworks.ring1.v1.TaskToActivityLink" %>



<div class="fieldcontain ${hasErrors(bean: taskToActivityLinkInstance, field: 'activity', 'error')} required">
	<label for="activity">
		<g:message code="taskToActivityLink.activity.label" default="Activity" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="activity" name="activity.id" from="${com.booleanworks.ring1.v1.Activity.list()}" optionKey="id" required="" value="${taskToActivityLinkInstance?.activity?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskToActivityLinkInstance, field: 'task', 'error')} required">
	<label for="task">
		<g:message code="taskToActivityLink.task.label" default="Task" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="task" name="task.id" from="${com.booleanworks.ring1.v1.Task.list()}" optionKey="id" required="" value="${taskToActivityLinkInstance?.task?.id}" class="many-to-one"/>
</div>

