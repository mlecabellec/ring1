<%@ page import="com.booleanworks.ring1.v1.Organization" %>



<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'parentOrganization', 'error')} ">
	<label for="parentOrganization">
		<g:message code="organization.parentOrganization.label" default="Parent Organization" />
		
	</label>
	<g:select id="parentOrganization" name="parentOrganization.id" from="${com.booleanworks.ring1.v1.Organization.list()}" optionKey="id" value="${organizationInstance?.parentOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'mainManager', 'error')} ">
	<label for="mainManager">
		<g:message code="organization.mainManager.label" default="Main Manager" />
		
	</label>
	<g:select id="mainManager" name="mainManager.id" from="${com.booleanworks.ring1.v1.Person.list()}" optionKey="id" value="${organizationInstance?.mainManager?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'deputyManager', 'error')} ">
	<label for="deputyManager">
		<g:message code="organization.deputyManager.label" default="Deputy Manager" />
		
	</label>
	<g:select id="deputyManager" name="deputyManager.id" from="${com.booleanworks.ring1.v1.Person.list()}" optionKey="id" value="${organizationInstance?.deputyManager?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'acronym', 'error')} ">
	<label for="acronym">
		<g:message code="organization.acronym.label" default="Acronym" />
		
	</label>
	<g:textField name="acronym" maxlength="32" value="${organizationInstance?.acronym}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'shortDescription', 'error')} required">
	<label for="shortDescription">
		<g:message code="organization.shortDescription.label" default="Short Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="shortDescription" maxlength="200" required="" value="${organizationInstance?.shortDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'longDescription', 'error')} ">
	<label for="longDescription">
		<g:message code="organization.longDescription.label" default="Long Description" />
		
	</label>
	<g:textArea name="longDescription" cols="40" rows="5" maxlength="4095" value="${organizationInstance?.longDescription}"/>
</div>

