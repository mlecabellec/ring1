
<%@ page import="com.booleanworks.ring1.v1.Organization" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'organization.label', default: 'Organization')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-organization" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-organization" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'organization.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'organization.lastUpdated.label', default: 'Last Updated')}" />
					
						<th><g:message code="organization.parentOrganization.label" default="Parent Organization" /></th>
					
						<th><g:message code="organization.mainManager.label" default="Main Manager" /></th>
					
						<th><g:message code="organization.deputyManager.label" default="Deputy Manager" /></th>
					
						<g:sortableColumn property="acronym" title="${message(code: 'organization.acronym.label', default: 'Acronym')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${organizationInstanceList}" status="i" var="organizationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${organizationInstance.id}">${fieldValue(bean: organizationInstance, field: "dateCreated")}</g:link></td>
					
						<td><g:formatDate date="${organizationInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: organizationInstance, field: "parentOrganization")}</td>
					
						<td>${fieldValue(bean: organizationInstance, field: "mainManager")}</td>
					
						<td>${fieldValue(bean: organizationInstance, field: "deputyManager")}</td>
					
						<td>${fieldValue(bean: organizationInstance, field: "acronym")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${organizationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
