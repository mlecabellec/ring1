
<%@ page import="com.booleanworks.ring1.v1.Organization" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'organization.label', default: 'Organization')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-organization" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-organization" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list organization">
			
				<g:if test="${organizationInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="organization.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${organizationInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="organization.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${organizationInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.parentOrganization}">
				<li class="fieldcontain">
					<span id="parentOrganization-label" class="property-label"><g:message code="organization.parentOrganization.label" default="Parent Organization" /></span>
					
						<span class="property-value" aria-labelledby="parentOrganization-label"><g:link controller="organization" action="show" id="${organizationInstance?.parentOrganization?.id}">${organizationInstance?.parentOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.mainManager}">
				<li class="fieldcontain">
					<span id="mainManager-label" class="property-label"><g:message code="organization.mainManager.label" default="Main Manager" /></span>
					
						<span class="property-value" aria-labelledby="mainManager-label"><g:link controller="person" action="show" id="${organizationInstance?.mainManager?.id}">${organizationInstance?.mainManager?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.deputyManager}">
				<li class="fieldcontain">
					<span id="deputyManager-label" class="property-label"><g:message code="organization.deputyManager.label" default="Deputy Manager" /></span>
					
						<span class="property-value" aria-labelledby="deputyManager-label"><g:link controller="person" action="show" id="${organizationInstance?.deputyManager?.id}">${organizationInstance?.deputyManager?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.acronym}">
				<li class="fieldcontain">
					<span id="acronym-label" class="property-label"><g:message code="organization.acronym.label" default="Acronym" /></span>
					
						<span class="property-value" aria-labelledby="acronym-label"><g:fieldValue bean="${organizationInstance}" field="acronym"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.shortDescription}">
				<li class="fieldcontain">
					<span id="shortDescription-label" class="property-label"><g:message code="organization.shortDescription.label" default="Short Description" /></span>
					
						<span class="property-value" aria-labelledby="shortDescription-label"><g:fieldValue bean="${organizationInstance}" field="shortDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.longDescription}">
				<li class="fieldcontain">
					<span id="longDescription-label" class="property-label"><g:message code="organization.longDescription.label" default="Long Description" /></span>
					
						<span class="property-value" aria-labelledby="longDescription-label"><g:fieldValue bean="${organizationInstance}" field="longDescription"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:organizationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${organizationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
