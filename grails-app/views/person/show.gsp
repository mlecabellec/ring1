
<%@ page import="com.booleanworks.ring1.v1.Person" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-person" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-person" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list person">
			
				<g:if test="${personInstance?.firstName}">
				<li class="fieldcontain">
					<span id="firstName-label" class="property-label"><g:message code="person.firstName.label" default="First Name" /></span>
					
						<span class="property-value" aria-labelledby="firstName-label"><g:fieldValue bean="${personInstance}" field="firstName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.lastName}">
				<li class="fieldcontain">
					<span id="lastName-label" class="property-label"><g:message code="person.lastName.label" default="Last Name" /></span>
					
						<span class="property-value" aria-labelledby="lastName-label"><g:fieldValue bean="${personInstance}" field="lastName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.fullName}">
				<li class="fieldcontain">
					<span id="fullName-label" class="property-label"><g:message code="person.fullName.label" default="Full Name" /></span>
					
						<span class="property-value" aria-labelledby="fullName-label"><g:fieldValue bean="${personInstance}" field="fullName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.mainEmail}">
				<li class="fieldcontain">
					<span id="mainEmail-label" class="property-label"><g:message code="person.mainEmail.label" default="Main Email" /></span>
					
						<span class="property-value" aria-labelledby="mainEmail-label"><g:fieldValue bean="${personInstance}" field="mainEmail"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.shortDescription}">
				<li class="fieldcontain">
					<span id="shortDescription-label" class="property-label"><g:message code="person.shortDescription.label" default="Short Description" /></span>
					
						<span class="property-value" aria-labelledby="shortDescription-label"><g:fieldValue bean="${personInstance}" field="shortDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.telephoneNumber}">
				<li class="fieldcontain">
					<span id="telephoneNumber-label" class="property-label"><g:message code="person.telephoneNumber.label" default="Telephone Number" /></span>
					
						<span class="property-value" aria-labelledby="telephoneNumber-label"><g:fieldValue bean="${personInstance}" field="telephoneNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.mobilePhone}">
				<li class="fieldcontain">
					<span id="mobilePhone-label" class="property-label"><g:message code="person.mobilePhone.label" default="Mobile Phone" /></span>
					
						<span class="property-value" aria-labelledby="mobilePhone-label"><g:fieldValue bean="${personInstance}" field="mobilePhone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.cn}">
				<li class="fieldcontain">
					<span id="cn-label" class="property-label"><g:message code="person.cn.label" default="Cn" /></span>
					
						<span class="property-value" aria-labelledby="cn-label"><g:fieldValue bean="${personInstance}" field="cn"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.dn}">
				<li class="fieldcontain">
					<span id="dn-label" class="property-label"><g:message code="person.dn.label" default="Dn" /></span>
					
						<span class="property-value" aria-labelledby="dn-label"><g:fieldValue bean="${personInstance}" field="dn"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.directoryUrl}">
				<li class="fieldcontain">
					<span id="directoryUrl-label" class="property-label"><g:message code="person.directoryUrl.label" default="Directory Url" /></span>
					
						<span class="property-value" aria-labelledby="directoryUrl-label"><g:fieldValue bean="${personInstance}" field="directoryUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.mainOrganization}">
				<li class="fieldcontain">
					<span id="mainOrganization-label" class="property-label"><g:message code="person.mainOrganization.label" default="Main Organization" /></span>
					
						<span class="property-value" aria-labelledby="mainOrganization-label"><g:link controller="organization" action="show" id="${personInstance?.mainOrganization?.id}">${personInstance?.mainOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="person.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${personInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="person.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${personInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.activityLinks}">
				<li class="fieldcontain">
					<span id="activityLinks-label" class="property-label"><g:message code="person.activityLinks.label" default="Activity Links" /></span>
					
						<g:each in="${personInstance.activityLinks}" var="a">
						<span class="property-value" aria-labelledby="activityLinks-label"><g:link controller="personToActivityLink" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:personInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${personInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
