<%@ page import="com.booleanworks.ring1.v1.Person" %>



<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="person.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" maxlength="128" required="" value="${personInstance?.firstName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="person.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" maxlength="128" required="" value="${personInstance?.lastName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'fullName', 'error')} required">
	<label for="fullName">
		<g:message code="person.fullName.label" default="Full Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fullName" maxlength="128" required="" value="${personInstance?.fullName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'mainEmail', 'error')} required">
	<label for="mainEmail">
		<g:message code="person.mainEmail.label" default="Main Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="mainEmail" maxlength="128" required="" value="${personInstance?.mainEmail}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'shortDescription', 'error')} ">
	<label for="shortDescription">
		<g:message code="person.shortDescription.label" default="Short Description" />
		
	</label>
	<g:textField name="shortDescription" maxlength="200" value="${personInstance?.shortDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'telephoneNumber', 'error')} ">
	<label for="telephoneNumber">
		<g:message code="person.telephoneNumber.label" default="Telephone Number" />
		
	</label>
	<g:textField name="telephoneNumber" maxlength="32" value="${personInstance?.telephoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'mobilePhone', 'error')} ">
	<label for="mobilePhone">
		<g:message code="person.mobilePhone.label" default="Mobile Phone" />
		
	</label>
	<g:textField name="mobilePhone" maxlength="32" value="${personInstance?.mobilePhone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'cn', 'error')} ">
	<label for="cn">
		<g:message code="person.cn.label" default="Cn" />
		
	</label>
	<g:textField name="cn" maxlength="128" value="${personInstance?.cn}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'dn', 'error')} ">
	<label for="dn">
		<g:message code="person.dn.label" default="Dn" />
		
	</label>
	<g:textField name="dn" maxlength="128" value="${personInstance?.dn}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'directoryUrl', 'error')} ">
	<label for="directoryUrl">
		<g:message code="person.directoryUrl.label" default="Directory Url" />
		
	</label>
	<g:textField name="directoryUrl" maxlength="128" value="${personInstance?.directoryUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'mainOrganization', 'error')} ">
	<label for="mainOrganization">
		<g:message code="person.mainOrganization.label" default="Main Organization" />
		
	</label>
	<g:select id="mainOrganization" name="mainOrganization.id" from="${com.booleanworks.ring1.v1.Organization.list()}" optionKey="id" value="${personInstance?.mainOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'activityLinks', 'error')} ">
	<label for="activityLinks">
		<g:message code="person.activityLinks.label" default="Activity Links" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${personInstance?.activityLinks?}" var="a">
    <li><g:link controller="personToActivityLink" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="personToActivityLink" action="create" params="['person.id': personInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'personToActivityLink.label', default: 'PersonToActivityLink')])}</g:link>
</li>
</ul>

</div>

