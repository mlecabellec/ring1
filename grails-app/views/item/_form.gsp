<%@ page import="com.booleanworks.ring1.v1.Item" %>



<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'shortDescription', 'error')} required">
	<label for="shortDescription">
		<g:message code="item.shortDescription.label" default="Short Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="shortDescription" maxlength="200" required="" value="${itemInstance?.shortDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'longDescription', 'error')} ">
	<label for="longDescription">
		<g:message code="item.longDescription.label" default="Long Description" />
		
	</label>
	<g:textArea name="longDescription" cols="40" rows="5" maxlength="4095" value="${itemInstance?.longDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'itemReference', 'error')} ">
	<label for="itemReference">
		<g:message code="item.itemReference.label" default="Item Reference" />
		
	</label>
	<g:textField name="itemReference" maxlength="200" value="${itemInstance?.itemReference}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'itemVersion', 'error')} ">
	<label for="itemVersion">
		<g:message code="item.itemVersion.label" default="Item Version" />
		
	</label>
	<g:textField name="itemVersion" maxlength="200" value="${itemInstance?.itemVersion}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'itemBatch', 'error')} ">
	<label for="itemBatch">
		<g:message code="item.itemBatch.label" default="Item Batch" />
		
	</label>
	<g:textField name="itemBatch" maxlength="200" value="${itemInstance?.itemBatch}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'itemSerialNumber', 'error')} ">
	<label for="itemSerialNumber">
		<g:message code="item.itemSerialNumber.label" default="Item Serial Number" />
		
	</label>
	<g:textField name="itemSerialNumber" maxlength="200" value="${itemInstance?.itemSerialNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'isTemplate', 'error')} ">
	<label for="isTemplate">
		<g:message code="item.isTemplate.label" default="Is Template" />
		
	</label>
	<g:checkBox name="isTemplate" value="${itemInstance?.isTemplate}" />
</div>

<div class="fieldcontain ${hasErrors(bean: itemInstance, field: 'isPhysicalObject', 'error')} ">
	<label for="isPhysicalObject">
		<g:message code="item.isPhysicalObject.label" default="Is Physical Object" />
		
	</label>
	<g:checkBox name="isPhysicalObject" value="${itemInstance?.isPhysicalObject}" />
</div>

