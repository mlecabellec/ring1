
<%@ page import="com.booleanworks.ring1.v1.Item" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-item" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-item" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list item">
			
				<g:if test="${itemInstance?.shortDescription}">
				<li class="fieldcontain">
					<span id="shortDescription-label" class="property-label"><g:message code="item.shortDescription.label" default="Short Description" /></span>
					
						<span class="property-value" aria-labelledby="shortDescription-label"><g:fieldValue bean="${itemInstance}" field="shortDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.longDescription}">
				<li class="fieldcontain">
					<span id="longDescription-label" class="property-label"><g:message code="item.longDescription.label" default="Long Description" /></span>
					
						<span class="property-value" aria-labelledby="longDescription-label"><g:fieldValue bean="${itemInstance}" field="longDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.itemReference}">
				<li class="fieldcontain">
					<span id="itemReference-label" class="property-label"><g:message code="item.itemReference.label" default="Item Reference" /></span>
					
						<span class="property-value" aria-labelledby="itemReference-label"><g:fieldValue bean="${itemInstance}" field="itemReference"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.itemVersion}">
				<li class="fieldcontain">
					<span id="itemVersion-label" class="property-label"><g:message code="item.itemVersion.label" default="Item Version" /></span>
					
						<span class="property-value" aria-labelledby="itemVersion-label"><g:fieldValue bean="${itemInstance}" field="itemVersion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.itemBatch}">
				<li class="fieldcontain">
					<span id="itemBatch-label" class="property-label"><g:message code="item.itemBatch.label" default="Item Batch" /></span>
					
						<span class="property-value" aria-labelledby="itemBatch-label"><g:fieldValue bean="${itemInstance}" field="itemBatch"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.itemSerialNumber}">
				<li class="fieldcontain">
					<span id="itemSerialNumber-label" class="property-label"><g:message code="item.itemSerialNumber.label" default="Item Serial Number" /></span>
					
						<span class="property-value" aria-labelledby="itemSerialNumber-label"><g:fieldValue bean="${itemInstance}" field="itemSerialNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.isTemplate}">
				<li class="fieldcontain">
					<span id="isTemplate-label" class="property-label"><g:message code="item.isTemplate.label" default="Is Template" /></span>
					
						<span class="property-value" aria-labelledby="isTemplate-label"><g:formatBoolean boolean="${itemInstance?.isTemplate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.isPhysicalObject}">
				<li class="fieldcontain">
					<span id="isPhysicalObject-label" class="property-label"><g:message code="item.isPhysicalObject.label" default="Is Physical Object" /></span>
					
						<span class="property-value" aria-labelledby="isPhysicalObject-label"><g:formatBoolean boolean="${itemInstance?.isPhysicalObject}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="item.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${itemInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${itemInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="item.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${itemInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:itemInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${itemInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
