
<%@ page import="com.booleanworks.ring1.v1.Item" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-item" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-item" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="shortDescription" title="${message(code: 'item.shortDescription.label', default: 'Short Description')}" />
					
						<g:sortableColumn property="longDescription" title="${message(code: 'item.longDescription.label', default: 'Long Description')}" />
					
						<g:sortableColumn property="itemReference" title="${message(code: 'item.itemReference.label', default: 'Item Reference')}" />
					
						<g:sortableColumn property="itemVersion" title="${message(code: 'item.itemVersion.label', default: 'Item Version')}" />
					
						<g:sortableColumn property="itemBatch" title="${message(code: 'item.itemBatch.label', default: 'Item Batch')}" />
					
						<g:sortableColumn property="itemSerialNumber" title="${message(code: 'item.itemSerialNumber.label', default: 'Item Serial Number')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${itemInstanceList}" status="i" var="itemInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${itemInstance.id}">${fieldValue(bean: itemInstance, field: "shortDescription")}</g:link></td>
					
						<td>${fieldValue(bean: itemInstance, field: "longDescription")}</td>
					
						<td>${fieldValue(bean: itemInstance, field: "itemReference")}</td>
					
						<td>${fieldValue(bean: itemInstance, field: "itemVersion")}</td>
					
						<td>${fieldValue(bean: itemInstance, field: "itemBatch")}</td>
					
						<td>${fieldValue(bean: itemInstance, field: "itemSerialNumber")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${itemInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
