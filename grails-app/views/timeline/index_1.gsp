
<%@ page import="com.booleanworks.ring1.v1.*" %>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>

        <title><g:message code="timeline.title" default="Timeline"  /></title>

        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.handsontable.full.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui-1.10.4.custom.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'docs.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'prettify.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'font-awesome.css')}" type="text/css"/>

        <g:javascript src="jquery-2.1.0.js"/>
        <g:javascript src="jquery-ui-1.10.4.custom.js"/>
        <g:javascript src="bootstrap.js" />
        <g:javascript src="jgestures.js" />
        <g:javascript src="jquery.handsontable.full.js" />
        <g:javascript library="application"/>
        <g:javascript src="holder.js" />
        <g:javascript src="prettify.js" />
        <g:javascript src="widgets.js" />
        <g:javascript src="d3.v3.js" />
        <g:javascript src="timeliner.js" />        

    <resource:richTextEditor type="medium"/>
    <ckeditor:resources/>

    <style>

        @media (min-width: 768px) {
        div#timelineTopContainer {
        margin-left:40px;
        border-left: 3px solid silver ;
        margin-top:7px ;
        margin-bottom:9px ;
        }

        li.timeline-item {
        margin-left:-54px;
        }

        li.timeline-item img.media-object {
        background-color:white;
        }
        }

        @media (min-width: 480px) and (max-width: 767px) {
        div#timelineTopContainer {
        margin-left:40px;
        /*border-left: 3px solid silver ;*/
        margin-top:7px ;
        margin-bottom:7px ;
        }

        li.timeline-item {
        margin-left:-55px;
        /*background-color:yellow;*/
        } 
        li.timeline-item img.media-object {
        background-color:white;
        }
        }

        @media (max-width: 479px) {
        div#timelineTopContainer {
        /*margin-left:40px;/*
        /*border-left: 3px solid silver ;*/
        margin-top:7px ;
        margin-bottom:7px ;
        }

        li.timeline-item {
        /*margin-left:-55px;*/
        /*background-color:pink;*/
        }   

        li.timeline-item img.media-object {
        display:none;
        }  
        }

    </style>    

</head>

<body id="mainBody">

    <div id="navBarContainer" class="container-fluid">
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

     <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

     <!-- Be sure to leave the brand out there if you want it shown -->
                    <a class="brand" href="#">Ring #1</a>

     <!-- Everything you want hidden at 940px or less, place within here -->
                    <div class="nav-collapse collapse">
                      <!-- .nav, .navbar-search, .navbar-form, etc -->
                        <ul class="nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">Link</a></li>
                            <li><a href="#">Link</a></li>
                        </ul>
                        <form class="navbar-search pull-right">
                            <input type="text" class="search-query" placeholder="Search">
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>





    <div id="contentTopContainer" class="container-fluid">
        <div id="mainLayoutRow" class="row-fluid">


            <div id="mainContentDiv" class="span12">
              <!--Body content-->


                <div id="threeColScaffoldingDiv" class="container-fluid">
                    <div class="row-fluid">
                        <div id="leftScaffoldingRow" class="span3">
                            <div class="well">
                                <form class="form-search">
                                    <div class="input-append">
                                        <input type="text" class="input-medium search-query">
                                        <button type="submit" class="btn">Search</button>
                                    </div>
                                </form>

                                <ul class="media-list">
                                    <li id="person_928"  class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'person_fontawesomeregular.svg')}"/>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">John DOE</h4>
                                            ESB Project manager
                                        </div>
                                        <script type="text/javascript">
                                            $( "#person_928").draggable();
                                        </script>                
                                    </li>
                                    <li id="request_928" class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'incoming_fontawesomeregular.svg')}"/>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">INC0002918002</h4>
                                            Unavailable ESB server
                                        </div>
                                        <script type="text/javascript">
                                            $( "#request_928").draggable();
                                        </script>                
                                    </li>  
                                    <li id="tag_45" class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'tag_fontawesomeregular.svg')}"/>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">ESB</h4>
                                            ESB tag
                                        </div>
                                        <script type="text/javascript">
                                            $( "#tag_45").draggable();
                                        </script>
                                    </li>
                                </ul>
                            </div>
                        </div>



                        <div id="centerScaffoldingRow" class="span6">

                            <div id="newActivityFormContainer" class="container-fluid">

                                <form id="newActivityForm">
                                    <fieldset class="form-horizontal" >
                                        <legend>New activity</legend>

                                        <div class="input-prepend">
                                            <span class="add-on">Short description</span>
                                            <input class="input-xxlarge" id="shortDescription" name="shortDescription" type="text" placeholder="Short description...">
                                        </div>                                        

                                    </fieldset >

                                    <fieldset style="margin-top:12px;">
                                        <label >Long description</label>
                                        <ckeditor:editor name="longDescription" height="110px" width="98.565%" toolbar="Basic">
                                            <Script> window.alert("bad test");</scRIpt>
                                        </ckeditor:editor>                                       
                                        <span class="help-block">Please write down the facts.</span>
                                    </fieldset>

                                    <fieldset class="form-inline"  style="margin-top:12px;" >

                                        <div class="input-prepend">
                                            <span class="add-on">Spent hours</span>
                                            <input class="input-medium" id="workHours" name="workHours" type="text" placeholder="REAL amount of hours">
                                        </div>  
                                        <label class="checkbox" >
                                            <input id="activiryDone" name="activityDone" type="checkbox" value="0">
                                            Done ?
                                        </label>
                                    </fieldset>

                                    <fieldset>
                                        <button type="submit" class="btn"  style="margin-top:12px;">Submit</button>
                                    </fieldset>
                                </form>
                            </div> 

                            <div id="timelineTopContainer" class="container-fluid">
                                <ul class="media-list">
                                    <li class="media timeline-item">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'gear_glyphicons_halflingsregular.svg')}"/>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">I did that
                                                <span class="label">tag</span>
                                                <span class="label label-success">Windchill award</span>
                                                <span class="label label-warning">Lie</span>
                                            </h4>
                                            <div class="media">
                                                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
                                                    blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
                                                    blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
                                                    blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah </p>

                                            </div>                                           
                                        </div>
                                    </li>
                                    <li class="media timeline-item">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'check1_glyphicons_halflingsregular.svg')}"/>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">I did that before
                                                <span class="label">tag</span>
                                                <span class="label label-success">tag</span>
                                                <span class="label label-warning">Lie</span>
                                            </h4>
                                            <div class="media">
                                                <p>blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
                                                    blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
                                                    blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah 
                                                    blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="media timeline-item">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'clock_glyphicons_halflingsregular.svg')}"/>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">I Saved the world
                                                <span class="label">Nobody know it</span>
                                                <span class="label label-success">Bruce Willis Prize</span>
                                                <span class="label label-warning">Lie</span>
                                            </h4>
                                            <div class="media">
                                                <p>Yes, I did it.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="media timeline-item">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" data-src="holder.js/64x64">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">I kicked Chuck Norris
                                                <span class="label">tag</span>
                                                <span class="label label-success">tag</span>
                                                <span class="label label-info">Chuck Norris</span>
                                                <span class="label label-warning">Lie</span>
                                            </h4>
                                            <div class="media">
                                                <p>Yes, I did. </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>


                            </div>


                        </div>
                        <div id="rightScaffoldingRow" class="span3">
                            <div class="well">
                                <form class="form-search">
                                    <div class="input-append">
                                        <input type="text" class="input-medium search-query">
                                        <button type="submit" class="btn">Search</button>
                                    </div>
                                </form>

                                <ul class="media-list">
                                    <li class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" data-src="holder.js/64x64">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">Media heading</h4>
                                            blah
                                        </div>
                                    </li>
                                    <li class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" data-src="holder.js/64x64">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">Media heading</h4>
                                            blah
                                        </div>
                                    </li>                                    
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>










</body>
</html>
