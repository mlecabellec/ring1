<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page import="com.booleanworks.ring1.v1.*" %>

<%@ page contentType="text/html;charset=UTF-8" %>

<li id="activity_${activityInstance.id}" class="media timeline-item">
    <a class="pull-left" href="#">
        <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'clock_glyphicons_halflingsregular.svg')}"/>
    </a>
    <div class="media-body">
        <h4 class="media-heading">${activityInstance.shortDescription}
            <span class="label">${activityInstance.realWorkHours}h</span>
            <g:each var="tagLink" in="${activityInstance.tagLinks}">
                <span id="tagToActivityLink_${tagLink.id}" class="label label-info">${tagLink.tag.defaultLabel}</span>
            </g:each>
            <g:each var="personLink" in="${activityInstance.personLinks}">
                <span id="personToActivityLink_${personLink.id}" class="label label-info">${personLink.person.fullName}</span>
            </g:each>
            <g:each var="taskLink" in="${activityInstance.taskLinks}">
                <span id="personToActivityLink_${taskLink.id}" class="label label-info">${taskLink.task.shortDescription}</span>
            </g:each>            
        </h4>
        <div class="media">
            <p>${raw(activityInstance.longDescription)} </p>

        </div>                                           
    </div>
    <script type="text/javascript">
        $( "#activity_${activityInstance.id}").droppable({
        drop: dropOnTimelineItemHandler, tolerance: "pointer", accept: ".searchresult-item"});
    </script>     
</li>
