
<%@ page import="com.booleanworks.ring1.v1.*" %>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>

        <title><g:message code="timeline.title" default="Timeline"  /></title>

        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.handsontable.full.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui-1.10.4.custom.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'docs.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'prettify.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'font-awesome.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'application-timeline.css')}" type="text/css"/>

        <!-- Manually managed scripts-->

        <g:javascript src="jquery-2.1.0.js"/>
        <g:javascript src="jquery-ui-1.10.4.custom.js"/>
        <g:javascript src="bootstrap.js" />
        <g:javascript src="jgestures.js" />
        <g:javascript src="jquery.handsontable.full.js" />
        <g:javascript library="application"/>
        <g:javascript src="holder.js" />
        <g:javascript src="prettify.js" />
        <g:javascript src="widgets.js" />
        <g:javascript src="d3.v3.js" />
        <g:javascript src="timeliner.js" />      

        <!-- resource:richTextEditor-->

    <resource:richTextEditor type="medium"/>

<!-- ckeditor:resources -->

    <ckeditor:resources/>

    <!-- r:layoutResources -->

    <r:layoutResources />

<!-- Page code -->

    <g:javascript src="application-timeline.js" /> 

</head>

<body id="mainBody">

    <div id="navBarContainer" class="container-fluid">
        <div class="navbar navbar-fixed-top" >
            <div class="navbar-inner">
                <div class="container">

     <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

     <!-- Be sure to leave the brand out there if you want it shown -->
                    <a class="brand" href="#">Ring #1</a>

     <!-- Everything you want hidden at 940px or less, place within here -->
                    <div class="nav-collapse collapse">
                      <!-- .nav, .navbar-search, .navbar-form, etc -->
                        <ul class="nav">
                            <li><a href="${createLink(uri: '/')}">Dashboard</a></li>
                            <li  class="active"><g:link controller="timeline">Timeline</g:link></li>
                            <li><g:link controller="ganttWorkshop">Plans</g:link></li>
                                <li><a href="#">Organizations</a></li>
                                <li><g:link controller="networkNavigator">Networks</g:link></li>
                                <li><a href="#">Messages</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Requests</a></li>
                            </ul>
                            <form class="navbar-search pull-right">
                                <input type="text" class="search-query" placeholder="Search">
                            </form>

                    </div>

                </div>
            </div>
        </div>
    </div>





    <div id="contentTopContainer" class="container-fluid">
        <div id="mainLayoutRow" class="row-fluid">


            <div id="mainContentDiv" class="span12">
              <!--Body content-->


                <div id="threeColScaffoldingDiv" class="container-fluid">
                    <div class="row-fluid">
                        <div id="leftScaffoldingRow" class="span3">
                            <div class="well">
                                <form class="form-search">
                                    <div class="">
                                        <button id="leftSearchButton" type="button" class="btn">Search</button>
                                        <input id="leftSearchBox" type="text" class="search-query ">                             
                                    </div>
                                </form>

                                <ul id="leftSearchResults" class="media-list">
                                    <g:include action="get_init_left_list"/>
                                </ul>
                            </div>
                        </div>



                        <div id="centerScaffoldingRow" class="span6">

                            <div id="newActivityFormContainer" class="container-fluid">

                                <form id="newActivityForm">
                                    <input type="hidden" name="id" value=""/>
                                    <input type="hidden" name="version" value=""/>
                                    <input type="hidden" name="mode" value=""/>
                                    <fieldset class="form-horizontal" >
                                        <legend>New activity</legend>

                                        <div class="input-prepend" style="width: 80%;">
                                            <span class="add-on">Short description</span>
                                            <input class="" style="width: 80%;" id="shortDescription" name="shortDescription" type="text" placeholder="Short description...">
                                        </div>                                        

                                    </fieldset >

                                    <fieldset style="margin-top:12px;">
                                        <label >Long description</label>
                                        <g:textArea id="longDescription" name="longDescription" value=""/>

                                        <span class="help-block">Please write down the facts.</span>
                                    </fieldset>

                                    <fieldset class="form-inline"  style="margin-top:12px;" >

                                        <div class="input-prepend">
                                            <span class="add-on">Spent hours</span>
                                            <input class="input-medium" id="workHours" name="workHours" type="text" placeholder="REAL amount of hours">
                                        </div>  

                                    </fieldset>

                                    <fieldset>
                                        <button id="submitNewActivityButton" type="button" class="btn"  style="margin-top:12px;">Submit</button>
                                    </fieldset>
                                </form>
                            </div> 

                            <div id="timelineTopContainer" class="container-fluid">
                                <ul id="timelineList" class="media-list">
                                    <g:include action="get_my_activity_timeline"  />
                                </ul>


                            </div>


                        </div>
                        <div id="rightScaffoldingRow" class="span3">
                            <div class="well">
                                <form class="form-search">
                                    <div class="">
                                        <input id="rightSearchBox" type="text" class="search-query">
                                        <button id="rightSearchButton" type="button" class="btn">Search</button>
                                    </div>
                                </form>

                                <ul id="rightSearchResults" class="media-list">
                                    <g:include action="get_init_right_list"/>
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

<r:layoutResources />

</body>
</html>
