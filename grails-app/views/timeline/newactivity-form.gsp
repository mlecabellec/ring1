<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page import="com.booleanworks.ring1.v1.*" %>

<%@ page contentType="text/html;charset=UTF-8" %>

<div id="centerScaffoldingRow" class="span6">

    <div id="newActivityFormContainer" class="container-fluid">

        <form id="newActivityForm">
            <fieldset class="form-horizontal" >
                <legend>New activity</legend>

                <div class="input-prepend">
                    <span class="add-on">Short description</span>
                    <input class="input-xxlarge" id="shortDescription" name="shortDescription" type="text" placeholder="Short description...">
                </div>                                        

            </fieldset >

            <fieldset style="margin-top:12px;">
                <label >Long description</label>
                <ckeditor:editor name="longDescription" height="110px" width="98.565%" toolbar="Basic">
                    <Script> window.alert("bad test");</scRIpt>
                </ckeditor:editor>                                       
                <span class="help-block">Please write down the facts.</span>
            </fieldset>

            <fieldset class="form-inline"  style="margin-top:12px;" >

                <div class="input-prepend">
                    <span class="add-on">Spent hours</span>
                    <input class="input-medium" id="workHours" name="workHours" type="text" placeholder="REAL amount of hours">
                </div>  
                <label class="checkbox" >
                    <input id="activiryDone" name="activityDone" type="checkbox" value="0">
                    Done ?
                </label>
            </fieldset>

            <fieldset>
                <button type="submit" class="btn"  style="margin-top:12px;">Submit</button>
            </fieldset>
        </form>
    </div>
