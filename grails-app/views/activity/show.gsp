
<%@ page import="com.booleanworks.ring1.v1.Activity" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'activity.label', default: 'Activity')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-activity" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-activity" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list activity">
			
				<g:if test="${activityInstance?.shortDescription}">
				<li class="fieldcontain">
					<span id="shortDescription-label" class="property-label"><g:message code="activity.shortDescription.label" default="Short Description" /></span>
					
						<span class="property-value" aria-labelledby="shortDescription-label"><g:fieldValue bean="${activityInstance}" field="shortDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.longDescription}">
				<li class="fieldcontain">
					<span id="longDescription-label" class="property-label"><g:message code="activity.longDescription.label" default="Long Description" /></span>
					
						<span class="property-value" aria-labelledby="longDescription-label"><g:fieldValue bean="${activityInstance}" field="longDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.owner}">
				<li class="fieldcontain">
					<span id="owner-label" class="property-label"><g:message code="activity.owner.label" default="Owner" /></span>
					
						<span class="property-value" aria-labelledby="owner-label"><g:link controller="person" action="show" id="${activityInstance?.owner?.id}">${activityInstance?.owner?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.realWorkHours}">
				<li class="fieldcontain">
					<span id="realWorkHours-label" class="property-label"><g:message code="activity.realWorkHours.label" default="Real Work Hours" /></span>
					
						<span class="property-value" aria-labelledby="realWorkHours-label"><g:fieldValue bean="${activityInstance}" field="realWorkHours"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="activity.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${activityInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="activity.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${activityInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.personLinks}">
				<li class="fieldcontain">
					<span id="personLinks-label" class="property-label"><g:message code="activity.personLinks.label" default="Person Links" /></span>
					
						<g:each in="${activityInstance.personLinks}" var="p">
						<span class="property-value" aria-labelledby="personLinks-label"><g:link controller="personToActivityLink" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.tagLinks}">
				<li class="fieldcontain">
					<span id="tagLinks-label" class="property-label"><g:message code="activity.tagLinks.label" default="Tag Links" /></span>
					
						<g:each in="${activityInstance.tagLinks}" var="t">
						<span class="property-value" aria-labelledby="tagLinks-label"><g:link controller="tagToActivityLink" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${activityInstance?.taskLinks}">
				<li class="fieldcontain">
					<span id="taskLinks-label" class="property-label"><g:message code="activity.taskLinks.label" default="Task Links" /></span>
					
						<g:each in="${activityInstance.taskLinks}" var="t">
						<span class="property-value" aria-labelledby="taskLinks-label"><g:link controller="taskToActivityLink" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:activityInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${activityInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
