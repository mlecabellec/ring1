<%@ page import="com.booleanworks.ring1.v1.Activity" %>



<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'shortDescription', 'error')} required">
	<label for="shortDescription">
		<g:message code="activity.shortDescription.label" default="Short Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="shortDescription" maxlength="200" required="" value="${activityInstance?.shortDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'longDescription', 'error')} ">
	<label for="longDescription">
		<g:message code="activity.longDescription.label" default="Long Description" />
		
	</label>
	<g:textArea name="longDescription" cols="40" rows="5" maxlength="4095" value="${activityInstance?.longDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'owner', 'error')} required">
	<label for="owner">
		<g:message code="activity.owner.label" default="Owner" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="owner" name="owner.id" from="${com.booleanworks.ring1.v1.Person.list()}" optionKey="id" required="" value="${activityInstance?.owner?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'realWorkHours', 'error')} required">
	<label for="realWorkHours">
		<g:message code="activity.realWorkHours.label" default="Real Work Hours" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="realWorkHours" value="${fieldValue(bean: activityInstance, field: 'realWorkHours')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'personLinks', 'error')} ">
	<label for="personLinks">
		<g:message code="activity.personLinks.label" default="Person Links" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${activityInstance?.personLinks?}" var="p">
    <li><g:link controller="personToActivityLink" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="personToActivityLink" action="create" params="['activity.id': activityInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'personToActivityLink.label', default: 'PersonToActivityLink')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'tagLinks', 'error')} ">
	<label for="tagLinks">
		<g:message code="activity.tagLinks.label" default="Tag Links" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${activityInstance?.tagLinks?}" var="t">
    <li><g:link controller="tagToActivityLink" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tagToActivityLink" action="create" params="['activity.id': activityInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'tagToActivityLink.label', default: 'TagToActivityLink')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: activityInstance, field: 'taskLinks', 'error')} ">
	<label for="taskLinks">
		<g:message code="activity.taskLinks.label" default="Task Links" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${activityInstance?.taskLinks?}" var="t">
    <li><g:link controller="taskToActivityLink" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="taskToActivityLink" action="create" params="['activity.id': activityInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'taskToActivityLink.label', default: 'TaskToActivityLink')])}</g:link>
</li>
</ul>

</div>

