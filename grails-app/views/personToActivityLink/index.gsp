
<%@ page import="com.booleanworks.ring1.v1.PersonToActivityLink" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'personToActivityLink.label', default: 'PersonToActivityLink')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-personToActivityLink" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-personToActivityLink" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="personToActivityLink.personToActivityLinkType.label" default="Person To Activity Link Type" /></th>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'personToActivityLink.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'personToActivityLink.lastUpdated.label', default: 'Last Updated')}" />
					
						<th><g:message code="personToActivityLink.activity.label" default="Activity" /></th>
					
						<th><g:message code="personToActivityLink.person.label" default="Person" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${personToActivityLinkInstanceList}" status="i" var="personToActivityLinkInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${personToActivityLinkInstance.id}">${fieldValue(bean: personToActivityLinkInstance, field: "personToActivityLinkType")}</g:link></td>
					
						<td><g:formatDate date="${personToActivityLinkInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${personToActivityLinkInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: personToActivityLinkInstance, field: "activity")}</td>
					
						<td>${fieldValue(bean: personToActivityLinkInstance, field: "person")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${personToActivityLinkInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
