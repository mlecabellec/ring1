<%@ page import="com.booleanworks.ring1.v1.PersonToActivityLink" %>



<div class="fieldcontain ${hasErrors(bean: personToActivityLinkInstance, field: 'personToActivityLinkType', 'error')} ">
	<label for="personToActivityLinkType">
		<g:message code="personToActivityLink.personToActivityLinkType.label" default="Person To Activity Link Type" />
		
	</label>
	<g:select id="personToActivityLinkType" name="personToActivityLinkType.id" from="${com.booleanworks.ring1.v1.PersonToActivityLinkType.list()}" optionKey="id" value="${personToActivityLinkInstance?.personToActivityLinkType?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personToActivityLinkInstance, field: 'activity', 'error')} required">
	<label for="activity">
		<g:message code="personToActivityLink.activity.label" default="Activity" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="activity" name="activity.id" from="${com.booleanworks.ring1.v1.Activity.list()}" optionKey="id" required="" value="${personToActivityLinkInstance?.activity?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personToActivityLinkInstance, field: 'person', 'error')} required">
	<label for="person">
		<g:message code="personToActivityLink.person.label" default="Person" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="person" name="person.id" from="${com.booleanworks.ring1.v1.Person.list()}" optionKey="id" required="" value="${personToActivityLinkInstance?.person?.id}" class="many-to-one"/>
</div>

