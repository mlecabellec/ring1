
<%@ page import="com.booleanworks.ring1.v1.PersonToActivityLink" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'personToActivityLink.label', default: 'PersonToActivityLink')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-personToActivityLink" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-personToActivityLink" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list personToActivityLink">
			
				<g:if test="${personToActivityLinkInstance?.personToActivityLinkType}">
				<li class="fieldcontain">
					<span id="personToActivityLinkType-label" class="property-label"><g:message code="personToActivityLink.personToActivityLinkType.label" default="Person To Activity Link Type" /></span>
					
						<span class="property-value" aria-labelledby="personToActivityLinkType-label"><g:link controller="personToActivityLinkType" action="show" id="${personToActivityLinkInstance?.personToActivityLinkType?.id}">${personToActivityLinkInstance?.personToActivityLinkType?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="personToActivityLink.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${personToActivityLinkInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="personToActivityLink.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${personToActivityLinkInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkInstance?.activity}">
				<li class="fieldcontain">
					<span id="activity-label" class="property-label"><g:message code="personToActivityLink.activity.label" default="Activity" /></span>
					
						<span class="property-value" aria-labelledby="activity-label"><g:link controller="activity" action="show" id="${personToActivityLinkInstance?.activity?.id}">${personToActivityLinkInstance?.activity?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkInstance?.person}">
				<li class="fieldcontain">
					<span id="person-label" class="property-label"><g:message code="personToActivityLink.person.label" default="Person" /></span>
					
						<span class="property-value" aria-labelledby="person-label"><g:link controller="person" action="show" id="${personToActivityLinkInstance?.person?.id}">${personToActivityLinkInstance?.person?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:personToActivityLinkInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${personToActivityLinkInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
