<%@ page import="com.booleanworks.ring1.v1.TagToItemLink" %>



<div class="fieldcontain ${hasErrors(bean: tagToItemLinkInstance, field: 'item', 'error')} required">
	<label for="item">
		<g:message code="tagToItemLink.item.label" default="Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="item" name="item.id" from="${com.booleanworks.ring1.v1.Item.list()}" optionKey="id" required="" value="${tagToItemLinkInstance?.item?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagToItemLinkInstance, field: 'tag', 'error')} required">
	<label for="tag">
		<g:message code="tagToItemLink.tag.label" default="Tag" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tag" name="tag.id" from="${com.booleanworks.ring1.v1.Tag.list()}" optionKey="id" required="" value="${tagToItemLinkInstance?.tag?.id}" class="many-to-one"/>
</div>

