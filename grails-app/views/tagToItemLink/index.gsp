
<%@ page import="com.booleanworks.ring1.v1.TagToItemLink" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tagToItemLink.label', default: 'TagToItemLink')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-tagToItemLink" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-tagToItemLink" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'tagToItemLink.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'tagToItemLink.lastUpdated.label', default: 'Last Updated')}" />
					
						<th><g:message code="tagToItemLink.item.label" default="Item" /></th>
					
						<th><g:message code="tagToItemLink.tag.label" default="Tag" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${tagToItemLinkInstanceList}" status="i" var="tagToItemLinkInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${tagToItemLinkInstance.id}">${fieldValue(bean: tagToItemLinkInstance, field: "dateCreated")}</g:link></td>
					
						<td><g:formatDate date="${tagToItemLinkInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: tagToItemLinkInstance, field: "item")}</td>
					
						<td>${fieldValue(bean: tagToItemLinkInstance, field: "tag")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${tagToItemLinkInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
