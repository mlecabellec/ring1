
<%@ page import="com.booleanworks.ring1.v1.*" %>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>

        <title><g:message code="ganttWorkshop.title" default="Plans"  /></title>

        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.handsontable.full.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui-1.10.4.custom.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'docs.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'prettify.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'font-awesome.css')}" type="text/css"/>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'application-timeline.css')}" type="text/css"/>

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'dhtmlxgantt.css')}" type="text/css"/>


<!-- Manually managed scripts-->

        <g:javascript src="jquery-2.1.0.js"/>
        <g:javascript src="jquery-ui-1.10.4.custom.js"/>
        <g:javascript src="bootstrap.js" />
        <g:javascript src="jgestures.js" />
        <g:javascript src="jquery.handsontable.full.js" />
        <g:javascript library="application"/>
        <g:javascript src="holder.js" />
        <g:javascript src="prettify.js" />
        <g:javascript src="widgets.js" />
        <g:javascript src="d3.v3.js" />
        <g:javascript src="timeliner.js" />  

        <g:javascript src="dhtmlxgantt.js" /> 




        <!-- resource:richTextEditor-->

    <resource:richTextEditor type="medium"/>

<!-- ckeditor:resources -->

    <ckeditor:resources/>

    <!-- r:layoutResources -->

    <r:layoutResources />

<!-- Page code -->

    <g:javascript src="application-plans.js" /> 

</head>

<body id="mainBody">

    <div id="navBarContainer" class="container-fluid">
        <div class="navbar navbar-fixed-top" >
            <div class="navbar-inner">
                <div class="container">

     <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

     <!-- Be sure to leave the brand out there if you want it shown -->
                    <a class="brand" href="#">Ring #1</a>

     <!-- Everything you want hidden at 940px or less, place within here -->
                    <div class="nav-collapse collapse">
                      <!-- .nav, .navbar-search, .navbar-form, etc -->
                        <ul class="nav">
                            <li><a href="${createLink(uri: '/')}">Dashboard</a></li>
                            <li><g:link controller="timeline">Timeline</g:link></li>
                                <li   class="active"><a href="#">Plans</a></li>
                                <li><a href="#">Organizations</a></li>
                                <li><g:link controller="networkNavigator">Relations</g:link></li>
                                <li><a href="#">Messages</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Requests</a></li>
                                <li><a href="#">Lists</a></li>
                            </ul>
                            <form class="navbar-search pull-right">
                                <input type="text" class="search-query" placeholder="Search">
                            </form>

                    </div>

                </div>
            </div>
        </div>
    </div>





    <div id="contentTopContainer" class="container-fluid">
        <div id="mainLayoutRow" class="row-fluid">


            <div id="mainContentDiv" class="span12">
              <!--Body content-->


                <div id="threeColScaffoldingDiv" class="container-fluid">
                    <div class="row-fluid">
                        <div id="leftScaffoldingRow" class="span3">
                            <div class="well">

                            </div>
                        </div>



                        <div id="centerScaffoldingRow" class="span6">

                            <div class="well">

                            </div>

                        </div>
                        <div id="rightScaffoldingRow" class="span3">
                            <div class="well">

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div id="ganttRow" class="row-fluid">


            <div id="gantContainer" class="span12">
              <!--Body content-->
                <div id="ganttWorkspace" style="padding:0px; overflow-y:auto; overflow-x:hidden; border:1px solid #e5e5e5; position:relative; margin:0 5px; width:1300px; height:800px;"></div>

            </div>

        </div>        
    </div>

<r:layoutResources />

</body>
</html>
