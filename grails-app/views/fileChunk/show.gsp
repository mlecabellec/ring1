
<%@ page import="com.booleanworks.ring1.v1.FileChunk" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fileChunk.label', default: 'FileChunk')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-fileChunk" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-fileChunk" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list fileChunk">
			
				<g:if test="${fileChunkInstance?.chunkSize}">
				<li class="fieldcontain">
					<span id="chunkSize-label" class="property-label"><g:message code="fileChunk.chunkSize.label" default="Chunk Size" /></span>
					
						<span class="property-value" aria-labelledby="chunkSize-label"><g:fieldValue bean="${fileChunkInstance}" field="chunkSize"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.chunkIndex}">
				<li class="fieldcontain">
					<span id="chunkIndex-label" class="property-label"><g:message code="fileChunk.chunkIndex.label" default="Chunk Index" /></span>
					
						<span class="property-value" aria-labelledby="chunkIndex-label"><g:fieldValue bean="${fileChunkInstance}" field="chunkIndex"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.previousChunk}">
				<li class="fieldcontain">
					<span id="previousChunk-label" class="property-label"><g:message code="fileChunk.previousChunk.label" default="Previous Chunk" /></span>
					
						<span class="property-value" aria-labelledby="previousChunk-label"><g:link controller="fileChunk" action="show" id="${fileChunkInstance?.previousChunk?.id}">${fileChunkInstance?.previousChunk?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.newtChunk}">
				<li class="fieldcontain">
					<span id="newtChunk-label" class="property-label"><g:message code="fileChunk.newtChunk.label" default="Newt Chunk" /></span>
					
						<span class="property-value" aria-labelledby="newtChunk-label"><g:link controller="fileChunk" action="show" id="${fileChunkInstance?.newtChunk?.id}">${fileChunkInstance?.newtChunk?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.payload}">
				<li class="fieldcontain">
					<span id="payload-label" class="property-label"><g:message code="fileChunk.payload.label" default="Payload" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.sha1Hash}">
				<li class="fieldcontain">
					<span id="sha1Hash-label" class="property-label"><g:message code="fileChunk.sha1Hash.label" default="Sha1 Hash" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="fileChunk.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${fileChunkInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="fileChunk.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${fileChunkInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileChunkInstance?.fileItem}">
				<li class="fieldcontain">
					<span id="fileItem-label" class="property-label"><g:message code="fileChunk.fileItem.label" default="File Item" /></span>
					
						<span class="property-value" aria-labelledby="fileItem-label"><g:link controller="fileItem" action="show" id="${fileChunkInstance?.fileItem?.id}">${fileChunkInstance?.fileItem?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:fileChunkInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${fileChunkInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
