<%@ page import="com.booleanworks.ring1.v1.FileChunk" %>



<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'chunkSize', 'error')} required">
	<label for="chunkSize">
		<g:message code="fileChunk.chunkSize.label" default="Chunk Size" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="chunkSize" type="number" value="${fileChunkInstance.chunkSize}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'chunkIndex', 'error')} required">
	<label for="chunkIndex">
		<g:message code="fileChunk.chunkIndex.label" default="Chunk Index" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="chunkIndex" type="number" value="${fileChunkInstance.chunkIndex}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'previousChunk', 'error')} ">
	<label for="previousChunk">
		<g:message code="fileChunk.previousChunk.label" default="Previous Chunk" />
		
	</label>
	<g:select id="previousChunk" name="previousChunk.id" from="${com.booleanworks.ring1.v1.FileChunk.list()}" optionKey="id" value="${fileChunkInstance?.previousChunk?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'newtChunk', 'error')} ">
	<label for="newtChunk">
		<g:message code="fileChunk.newtChunk.label" default="Newt Chunk" />
		
	</label>
	<g:select id="newtChunk" name="newtChunk.id" from="${com.booleanworks.ring1.v1.FileChunk.list()}" optionKey="id" value="${fileChunkInstance?.newtChunk?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'payload', 'error')} required">
	<label for="payload">
		<g:message code="fileChunk.payload.label" default="Payload" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="payload" name="payload" />
</div>

<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'sha1Hash', 'error')} required">
	<label for="sha1Hash">
		<g:message code="fileChunk.sha1Hash.label" default="Sha1 Hash" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="sha1Hash" name="sha1Hash" />
</div>

<div class="fieldcontain ${hasErrors(bean: fileChunkInstance, field: 'fileItem', 'error')} required">
	<label for="fileItem">
		<g:message code="fileChunk.fileItem.label" default="File Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="fileItem" name="fileItem.id" from="${com.booleanworks.ring1.v1.FileItem.list()}" optionKey="id" required="" value="${fileChunkInstance?.fileItem?.id}" class="many-to-one"/>
</div>

