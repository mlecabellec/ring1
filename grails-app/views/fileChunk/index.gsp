
<%@ page import="com.booleanworks.ring1.v1.FileChunk" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fileChunk.label', default: 'FileChunk')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-fileChunk" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-fileChunk" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="chunkSize" title="${message(code: 'fileChunk.chunkSize.label', default: 'Chunk Size')}" />
					
						<g:sortableColumn property="chunkIndex" title="${message(code: 'fileChunk.chunkIndex.label', default: 'Chunk Index')}" />
					
						<th><g:message code="fileChunk.previousChunk.label" default="Previous Chunk" /></th>
					
						<th><g:message code="fileChunk.newtChunk.label" default="Newt Chunk" /></th>
					
						<g:sortableColumn property="payload" title="${message(code: 'fileChunk.payload.label', default: 'Payload')}" />
					
						<g:sortableColumn property="sha1Hash" title="${message(code: 'fileChunk.sha1Hash.label', default: 'Sha1 Hash')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${fileChunkInstanceList}" status="i" var="fileChunkInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${fileChunkInstance.id}">${fieldValue(bean: fileChunkInstance, field: "chunkSize")}</g:link></td>
					
						<td>${fieldValue(bean: fileChunkInstance, field: "chunkIndex")}</td>
					
						<td>${fieldValue(bean: fileChunkInstance, field: "previousChunk")}</td>
					
						<td>${fieldValue(bean: fileChunkInstance, field: "newtChunk")}</td>
					
						<td>${fieldValue(bean: fileChunkInstance, field: "payload")}</td>
					
						<td>${fieldValue(bean: fileChunkInstance, field: "sha1Hash")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${fileChunkInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
