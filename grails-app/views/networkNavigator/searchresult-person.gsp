<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page import="com.booleanworks.ring1.v1.*" %>

<%@ page contentType="text/html;charset=UTF-8" %>

<li id="person_${person.id}"  class="media searchresult-item">
    <a class="pull-left" href="#">
        <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'person_fontawesomeregular.svg')}"/>
    </a>
    <div class="media-body">
        <h4 class="media-heading">${person.fullName}</h4>
        ${person?.shortDescription}
    </div>              
</li>
