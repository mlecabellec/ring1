<%@ page import="com.booleanworks.ring1.v1.*" %>

<%@ page contentType="text/html;charset=UTF-8" %>



<g:each var="currentResult" in="${results}">
    <g:if test="${currentResult.getClass().isAssignableFrom(Tag.class)}">
        <g:include action="get_search_result_tag" id="${currentResult.id}" />
    </g:if>
    <g:elseif test="${currentResult.getClass().isAssignableFrom(Person.class)}">
        <g:include action="get_search_result_person" id="${currentResult.id}" />
    </g:elseif>
    <g:elseif test="${currentResult.getClass().isAssignableFrom(Task.class)}">
        <g:include action="get_search_result_task" id="${currentResult.id}" />
    </g:elseif>    <g:else>
        <li id="object_${currentResult.id}"  class="media searchresult-item" style="display:none;">
            <a class="pull-left" href="#">
                <img class="media-object" width="64" height="64" src="${resource(dir: 'images', file: 'question1_glyphicons_halflingsregular.svg')}"/>
            </a>
            <div class="media-body">
                <h4 class="media-heading">Uncharted object</h4>
                ${currentResult.toString()}
            </div>             
        </li>            
    </g:else>
</g:each>


