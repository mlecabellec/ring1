<%@ page import="com.booleanworks.ring1.v1.Tag" %>



<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'defaultLabel', 'error')} required">
	<label for="defaultLabel">
		<g:message code="tag.defaultLabel.label" default="Default Label" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="defaultLabel" maxlength="200" required="" value="${tagInstance?.defaultLabel}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'labelCode', 'error')} required">
	<label for="labelCode">
		<g:message code="tag.labelCode.label" default="Label Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="labelCode" maxlength="200" required="" value="${tagInstance?.labelCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'localeInfo', 'error')} ">
	<label for="localeInfo">
		<g:message code="tag.localeInfo.label" default="Locale Info" />
		
	</label>
	<g:textField name="localeInfo" maxlength="64" value="${tagInstance?.localeInfo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'parent', 'error')} ">
	<label for="parent">
		<g:message code="tag.parent.label" default="Parent" />
		
	</label>
	<g:select id="parent" name="parent.id" from="${com.booleanworks.ring1.v1.Tag.list()}" optionKey="id" value="${tagInstance?.parent?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'activityLinks', 'error')} ">
	<label for="activityLinks">
		<g:message code="tag.activityLinks.label" default="Activity Links" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${tagInstance?.activityLinks?}" var="a">
    <li><g:link controller="tagToActivityLink" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tagToActivityLink" action="create" params="['tag.id': tagInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'tagToActivityLink.label', default: 'TagToActivityLink')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'itemLinks', 'error')} ">
	<label for="itemLinks">
		<g:message code="tag.itemLinks.label" default="Item Links" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${tagInstance?.itemLinks?}" var="i">
    <li><g:link controller="tagToItemLink" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tagToItemLink" action="create" params="['tag.id': tagInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'tagToItemLink.label', default: 'TagToItemLink')])}</g:link>
</li>
</ul>

</div>

