
<%@ page import="com.booleanworks.ring1.v1.Tag" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tag" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tag">
			
				<g:if test="${tagInstance?.defaultLabel}">
				<li class="fieldcontain">
					<span id="defaultLabel-label" class="property-label"><g:message code="tag.defaultLabel.label" default="Default Label" /></span>
					
						<span class="property-value" aria-labelledby="defaultLabel-label"><g:fieldValue bean="${tagInstance}" field="defaultLabel"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.labelCode}">
				<li class="fieldcontain">
					<span id="labelCode-label" class="property-label"><g:message code="tag.labelCode.label" default="Label Code" /></span>
					
						<span class="property-value" aria-labelledby="labelCode-label"><g:fieldValue bean="${tagInstance}" field="labelCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.localeInfo}">
				<li class="fieldcontain">
					<span id="localeInfo-label" class="property-label"><g:message code="tag.localeInfo.label" default="Locale Info" /></span>
					
						<span class="property-value" aria-labelledby="localeInfo-label"><g:fieldValue bean="${tagInstance}" field="localeInfo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.parent}">
				<li class="fieldcontain">
					<span id="parent-label" class="property-label"><g:message code="tag.parent.label" default="Parent" /></span>
					
						<span class="property-value" aria-labelledby="parent-label"><g:link controller="tag" action="show" id="${tagInstance?.parent?.id}">${tagInstance?.parent?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="tag.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${tagInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="tag.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${tagInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.activityLinks}">
				<li class="fieldcontain">
					<span id="activityLinks-label" class="property-label"><g:message code="tag.activityLinks.label" default="Activity Links" /></span>
					
						<g:each in="${tagInstance.activityLinks}" var="a">
						<span class="property-value" aria-labelledby="activityLinks-label"><g:link controller="tagToActivityLink" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${tagInstance?.itemLinks}">
				<li class="fieldcontain">
					<span id="itemLinks-label" class="property-label"><g:message code="tag.itemLinks.label" default="Item Links" /></span>
					
						<g:each in="${tagInstance.itemLinks}" var="i">
						<span class="property-value" aria-labelledby="itemLinks-label"><g:link controller="tagToItemLink" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tagInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
