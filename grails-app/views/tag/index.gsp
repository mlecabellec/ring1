
<%@ page import="com.booleanworks.ring1.v1.Tag" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-tag" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="defaultLabel" title="${message(code: 'tag.defaultLabel.label', default: 'Default Label')}" />
					
						<g:sortableColumn property="labelCode" title="${message(code: 'tag.labelCode.label', default: 'Label Code')}" />
					
						<g:sortableColumn property="localeInfo" title="${message(code: 'tag.localeInfo.label', default: 'Locale Info')}" />
					
						<th><g:message code="tag.parent.label" default="Parent" /></th>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'tag.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'tag.lastUpdated.label', default: 'Last Updated')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${tagInstanceList}" status="i" var="tagInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${tagInstance.id}">${fieldValue(bean: tagInstance, field: "defaultLabel")}</g:link></td>
					
						<td>${fieldValue(bean: tagInstance, field: "labelCode")}</td>
					
						<td>${fieldValue(bean: tagInstance, field: "localeInfo")}</td>
					
						<td>${fieldValue(bean: tagInstance, field: "parent")}</td>
					
						<td><g:formatDate date="${tagInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${tagInstance.lastUpdated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${tagInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
