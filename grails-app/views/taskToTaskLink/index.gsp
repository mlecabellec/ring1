
<%@ page import="com.booleanworks.ring1.v1.TaskToTaskLink" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'taskToTaskLink.label', default: 'TaskToTaskLink')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-taskToTaskLink" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-taskToTaskLink" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'taskToTaskLink.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'taskToTaskLink.lastUpdated.label', default: 'Last Updated')}" />
					
						<th><g:message code="taskToTaskLink.destinationTask.label" default="Destination Task" /></th>
					
						<th><g:message code="taskToTaskLink.sourceTask.label" default="Source Task" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${taskToTaskLinkInstanceList}" status="i" var="taskToTaskLinkInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${taskToTaskLinkInstance.id}">${fieldValue(bean: taskToTaskLinkInstance, field: "dateCreated")}</g:link></td>
					
						<td><g:formatDate date="${taskToTaskLinkInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: taskToTaskLinkInstance, field: "destinationTask")}</td>
					
						<td>${fieldValue(bean: taskToTaskLinkInstance, field: "sourceTask")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${taskToTaskLinkInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
