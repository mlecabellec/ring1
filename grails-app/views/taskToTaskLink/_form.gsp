<%@ page import="com.booleanworks.ring1.v1.TaskToTaskLink" %>



<div class="fieldcontain ${hasErrors(bean: taskToTaskLinkInstance, field: 'destinationTask', 'error')} required">
	<label for="destinationTask">
		<g:message code="taskToTaskLink.destinationTask.label" default="Destination Task" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="destinationTask" name="destinationTask.id" from="${com.booleanworks.ring1.v1.Task.list()}" optionKey="id" required="" value="${taskToTaskLinkInstance?.destinationTask?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskToTaskLinkInstance, field: 'sourceTask', 'error')} required">
	<label for="sourceTask">
		<g:message code="taskToTaskLink.sourceTask.label" default="Source Task" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="sourceTask" name="sourceTask.id" from="${com.booleanworks.ring1.v1.Task.list()}" optionKey="id" required="" value="${taskToTaskLinkInstance?.sourceTask?.id}" class="many-to-one"/>
</div>

