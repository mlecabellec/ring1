<%@ page import="com.booleanworks.ring1.v1.FileItem" %>



<div class="fieldcontain ${hasErrors(bean: fileItemInstance, field: 'fileName', 'error')} required">
	<label for="fileName">
		<g:message code="fileItem.fileName.label" default="File Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fileName" maxlength="200" required="" value="${fileItemInstance?.fileName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileItemInstance, field: 'virtualCanonicalPath', 'error')} required">
	<label for="virtualCanonicalPath">
		<g:message code="fileItem.virtualCanonicalPath.label" default="Virtual Canonical Path" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="virtualCanonicalPath" cols="40" rows="5" maxlength="2000" required="" value="${fileItemInstance?.virtualCanonicalPath}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileItemInstance, field: 'chunks', 'error')} ">
	<label for="chunks">
		<g:message code="fileItem.chunks.label" default="Chunks" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${fileItemInstance?.chunks?}" var="c">
    <li><g:link controller="fileChunk" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="fileChunk" action="create" params="['fileItem.id': fileItemInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'fileChunk.label', default: 'FileChunk')])}</g:link>
</li>
</ul>

</div>

