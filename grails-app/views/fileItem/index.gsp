
<%@ page import="com.booleanworks.ring1.v1.FileItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fileItem.label', default: 'FileItem')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-fileItem" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-fileItem" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="fileName" title="${message(code: 'fileItem.fileName.label', default: 'File Name')}" />
					
						<g:sortableColumn property="virtualCanonicalPath" title="${message(code: 'fileItem.virtualCanonicalPath.label', default: 'Virtual Canonical Path')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'fileItem.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'fileItem.lastUpdated.label', default: 'Last Updated')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${fileItemInstanceList}" status="i" var="fileItemInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${fileItemInstance.id}">${fieldValue(bean: fileItemInstance, field: "fileName")}</g:link></td>
					
						<td>${fieldValue(bean: fileItemInstance, field: "virtualCanonicalPath")}</td>
					
						<td><g:formatDate date="${fileItemInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${fileItemInstance.lastUpdated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${fileItemInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
