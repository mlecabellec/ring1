
<%@ page import="com.booleanworks.ring1.v1.FileItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fileItem.label', default: 'FileItem')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-fileItem" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-fileItem" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list fileItem">
			
				<g:if test="${fileItemInstance?.fileName}">
				<li class="fieldcontain">
					<span id="fileName-label" class="property-label"><g:message code="fileItem.fileName.label" default="File Name" /></span>
					
						<span class="property-value" aria-labelledby="fileName-label"><g:fieldValue bean="${fileItemInstance}" field="fileName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileItemInstance?.virtualCanonicalPath}">
				<li class="fieldcontain">
					<span id="virtualCanonicalPath-label" class="property-label"><g:message code="fileItem.virtualCanonicalPath.label" default="Virtual Canonical Path" /></span>
					
						<span class="property-value" aria-labelledby="virtualCanonicalPath-label"><g:fieldValue bean="${fileItemInstance}" field="virtualCanonicalPath"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileItemInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="fileItem.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${fileItemInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileItemInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="fileItem.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${fileItemInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${fileItemInstance?.chunks}">
				<li class="fieldcontain">
					<span id="chunks-label" class="property-label"><g:message code="fileItem.chunks.label" default="Chunks" /></span>
					
						<g:each in="${fileItemInstance.chunks}" var="c">
						<span class="property-value" aria-labelledby="chunks-label"><g:link controller="fileChunk" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:fileItemInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${fileItemInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
