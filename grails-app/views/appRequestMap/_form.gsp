<%@ page import="com.booleanworks.ring1.v1.AppRequestMap" %>



<div class="fieldcontain ${hasErrors(bean: appRequestMapInstance, field: 'url', 'error')} required">
	<label for="url">
		<g:message code="appRequestMap.url.label" default="Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="url" required="" value="${appRequestMapInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appRequestMapInstance, field: 'configAttribute', 'error')} required">
	<label for="configAttribute">
		<g:message code="appRequestMap.configAttribute.label" default="Config Attribute" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="configAttribute" required="" value="${appRequestMapInstance?.configAttribute}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: appRequestMapInstance, field: 'httpMethod', 'error')} ">
	<label for="httpMethod">
		<g:message code="appRequestMap.httpMethod.label" default="Http Method" />
		
	</label>
	<g:select name="httpMethod" from="${org.springframework.http.HttpMethod?.values()}" keys="${org.springframework.http.HttpMethod.values()*.name()}" value="${appRequestMapInstance?.httpMethod?.name()}" noSelection="['': '']"/>
</div>

