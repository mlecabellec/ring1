<%@ page import="com.booleanworks.ring1.v1.TagToActivityLink" %>



<div class="fieldcontain ${hasErrors(bean: tagToActivityLinkInstance, field: 'activity', 'error')} required">
	<label for="activity">
		<g:message code="tagToActivityLink.activity.label" default="Activity" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="activity" name="activity.id" from="${com.booleanworks.ring1.v1.Activity.list()}" optionKey="id" required="" value="${tagToActivityLinkInstance?.activity?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagToActivityLinkInstance, field: 'tag', 'error')} required">
	<label for="tag">
		<g:message code="tagToActivityLink.tag.label" default="Tag" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tag" name="tag.id" from="${com.booleanworks.ring1.v1.Tag.list()}" optionKey="id" required="" value="${tagToActivityLinkInstance?.tag?.id}" class="many-to-one"/>
</div>

