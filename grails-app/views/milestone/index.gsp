
<%@ page import="com.booleanworks.ring1.v1.Milestone" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'milestone.label', default: 'Milestone')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-milestone" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-milestone" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="shortDescription" title="${message(code: 'milestone.shortDescription.label', default: 'Short Description')}" />
					
						<g:sortableColumn property="longDescription" title="${message(code: 'milestone.longDescription.label', default: 'Long Description')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'milestone.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'milestone.lastUpdated.label', default: 'Last Updated')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${milestoneInstanceList}" status="i" var="milestoneInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${milestoneInstance.id}">${fieldValue(bean: milestoneInstance, field: "shortDescription")}</g:link></td>
					
						<td>${fieldValue(bean: milestoneInstance, field: "longDescription")}</td>
					
						<td><g:formatDate date="${milestoneInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${milestoneInstance.lastUpdated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${milestoneInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
