<%@ page import="com.booleanworks.ring1.v1.Milestone" %>



<div class="fieldcontain ${hasErrors(bean: milestoneInstance, field: 'shortDescription', 'error')} required">
	<label for="shortDescription">
		<g:message code="milestone.shortDescription.label" default="Short Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="shortDescription" maxlength="200" required="" value="${milestoneInstance?.shortDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: milestoneInstance, field: 'longDescription', 'error')} ">
	<label for="longDescription">
		<g:message code="milestone.longDescription.label" default="Long Description" />
		
	</label>
	<g:textArea name="longDescription" cols="40" rows="5" maxlength="4095" value="${milestoneInstance?.longDescription}"/>
</div>

