
<%@ page import="com.booleanworks.ring1.v1.PersonToActivityLinkType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'personToActivityLinkType.label', default: 'PersonToActivityLinkType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-personToActivityLinkType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-personToActivityLinkType" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="defaultLabel" title="${message(code: 'personToActivityLinkType.defaultLabel.label', default: 'Default Label')}" />
					
						<g:sortableColumn property="labelCode" title="${message(code: 'personToActivityLinkType.labelCode.label', default: 'Label Code')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'personToActivityLinkType.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'personToActivityLinkType.lastUpdated.label', default: 'Last Updated')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${personToActivityLinkTypeInstanceList}" status="i" var="personToActivityLinkTypeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${personToActivityLinkTypeInstance.id}">${fieldValue(bean: personToActivityLinkTypeInstance, field: "defaultLabel")}</g:link></td>
					
						<td>${fieldValue(bean: personToActivityLinkTypeInstance, field: "labelCode")}</td>
					
						<td><g:formatDate date="${personToActivityLinkTypeInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${personToActivityLinkTypeInstance.lastUpdated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${personToActivityLinkTypeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
