
<%@ page import="com.booleanworks.ring1.v1.PersonToActivityLinkType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'personToActivityLinkType.label', default: 'PersonToActivityLinkType')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-personToActivityLinkType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-personToActivityLinkType" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list personToActivityLinkType">
			
				<g:if test="${personToActivityLinkTypeInstance?.defaultLabel}">
				<li class="fieldcontain">
					<span id="defaultLabel-label" class="property-label"><g:message code="personToActivityLinkType.defaultLabel.label" default="Default Label" /></span>
					
						<span class="property-value" aria-labelledby="defaultLabel-label"><g:fieldValue bean="${personToActivityLinkTypeInstance}" field="defaultLabel"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkTypeInstance?.labelCode}">
				<li class="fieldcontain">
					<span id="labelCode-label" class="property-label"><g:message code="personToActivityLinkType.labelCode.label" default="Label Code" /></span>
					
						<span class="property-value" aria-labelledby="labelCode-label"><g:fieldValue bean="${personToActivityLinkTypeInstance}" field="labelCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkTypeInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="personToActivityLinkType.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${personToActivityLinkTypeInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personToActivityLinkTypeInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="personToActivityLinkType.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${personToActivityLinkTypeInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:personToActivityLinkTypeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${personToActivityLinkTypeInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
