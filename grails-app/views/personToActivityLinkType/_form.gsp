<%@ page import="com.booleanworks.ring1.v1.PersonToActivityLinkType" %>



<div class="fieldcontain ${hasErrors(bean: personToActivityLinkTypeInstance, field: 'defaultLabel', 'error')} ">
	<label for="defaultLabel">
		<g:message code="personToActivityLinkType.defaultLabel.label" default="Default Label" />
		
	</label>
	<g:textField name="defaultLabel" value="${personToActivityLinkTypeInstance?.defaultLabel}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personToActivityLinkTypeInstance, field: 'labelCode', 'error')} ">
	<label for="labelCode">
		<g:message code="personToActivityLinkType.labelCode.label" default="Label Code" />
		
	</label>
	<g:textField name="labelCode" value="${personToActivityLinkTypeInstance?.labelCode}"/>
</div>

