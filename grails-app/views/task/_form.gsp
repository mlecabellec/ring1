<%@ page import="com.booleanworks.ring1.v1.Task" %>



<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'shortDescription', 'error')} required">
	<label for="shortDescription">
		<g:message code="task.shortDescription.label" default="Short Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="shortDescription" maxlength="200" required="" value="${taskInstance?.shortDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'longDescription', 'error')} ">
	<label for="longDescription">
		<g:message code="task.longDescription.label" default="Long Description" />
		
	</label>
	<g:textArea name="longDescription" cols="40" rows="5" maxlength="4095" value="${taskInstance?.longDescription}"/>
</div>

