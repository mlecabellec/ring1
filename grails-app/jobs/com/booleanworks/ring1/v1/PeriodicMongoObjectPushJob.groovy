/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.booleanworks.ring1.v1

import grails.converters.JSON


/**
 *
 * @author vortigern
 */
class PeriodicMongoObjectPushJob {
    static triggers = {
        cron name: 'periodicMongoObjectPushJobTrigger', cronExpression: "0 * * * * ?"
    }
    def group = "indexMaintenance"
 
    def sessionRequired = true
    
    def concurrent = false
 
    def searchableService
    //def elasticSearchService
    
    def mongoSyncUtilityService
    
    def mongo
    
    def execute(){
        print "PeriodicMongoObjectPushJob.execute()..."
        //elasticSearchService.index()
        

        mongoSyncUtilityService.pushLastActivities()

        
        print "PeriodicMongoObjectPushJob.execute() ended."
    }	
}

