/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.booleanworks.ring1.v1

/**
 *
 * @author vortigern
 */
class PerodicIndexUpdaterJob {
    static triggers = {
        cron name: 'perodicIndexUpdaterJobTrigger', cronExpression: "0 0 * * * ?"
    }
    def group = "indexMaintenance"
 
    def sessionRequired = true
    
    def concurrent = false
 
    def searchableService
    //def elasticSearchService
    
    def execute(){
        print "PerodicIndexUpdaterJob.execute()..."
        //elasticSearchService.index()
        
        int maxBurstSize = 50
        
        Task.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        Person.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        Tag.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        Activity.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        Item.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        Milestone.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        Organization.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        FileItem.listOrderByLastUpdated(max:maxBurstSize,order:"desc").each(){ it -> it.index()}
        
        print "PerodicIndexUpdaterJob.execute() ended."
    }	
}

