import com.booleanworks.ring1.v1.*

import grails.util.Environment

class BootStrap {

    def init = { servletContext ->

        Organization univers = Organization.findOrSaveWhere(shortDescription:"Univers",longDescription:"The Universe is commonly defined as the totality of existence.")
        
        
        if(AppUser.findByUsername("superadmin") == null)
        {
            AppUser superadmin = new AppUser() 
            superadmin.setUsername("superadmin")
            
            Random random = new Random()
            String bootstrapPassword = Long.toHexString(random.nextLong() * random.nextLong())
            bootstrapPassword += Long.toHexString(random.nextLong() * random.nextLong())

            //TODO: Change me
            bootstrapPassword = "ChangeMe"            
            
            System.out.println("super: " + bootstrapPassword)
            
            superadmin.setPassword(bootstrapPassword)
            superadmin.setUsername("superadmin")
            superadmin.setAccountExpired(false)
            superadmin.setAccountLocked(false)
            superadmin.setEnabled(true)
            superadmin.setPasswordExpired(false)
            superadmin.save(flush:true,failOnError:true)
        }
        else
        {
            
            AppUser superadmin = AppUser.findByUsername("superadmin")
            Random random = new Random()
            String bootstrapPassword = Long.toHexString(random.nextLong() * random.nextLong())
            bootstrapPassword += Long.toHexString(random.nextLong() * random.nextLong())
           
            bootstrapPassword = "ChangeMe" 
            
            System.out.println("super: " + bootstrapPassword)
            
            superadmin.setPassword(bootstrapPassword)
            superadmin.save(flush:true,failOnError:true)
             
        }
        
        if(AppRole.findByAuthority("SUPER_ADMIN") == null)
        {
            AppRole superadmin = new AppRole()
            superadmin.setAuthority("SUPER_ADMIN")
            superadmin.save(flush:true,failOnError:true)
          
            
        }
      
        if(AppRole.findByAuthority("SIMPLE_USER") == null)
        {
            AppRole superadmin = new AppRole()
            superadmin.setAuthority("SIMPLE_USER")
            superadmin.save(flush:true,failOnError:true)
          
            
        }        
      
        if(AppUser.findByUsername("superadmin") != null && AppRole.findByAuthority("SUPER_ADMIN") != null && AppUserAppRole.findWhere(appUser:AppUser.findByUsername("superadmin"), appRole:AppRole.findByAuthority("SUPER_ADMIN")) == null)
        {
            AppUserAppRole.create(AppUser.findByUsername("superadmin"),AppRole.findByAuthority("SUPER_ADMIN"),true) ;
        }
        
        if(AppUser.findByUsername("superadmin") != null && AppRole.findByAuthority("SIMPLE_USER") != null && AppUserAppRole.findWhere(appUser:AppUser.findByUsername("superadmin"), appRole:AppRole.findByAuthority("SIMPLE_USER")) == null)
        {
            AppUserAppRole.create(AppUser.findByUsername("superadmin"),AppRole.findByAuthority("SIMPLE_USER"),true) ;
        } 
        
        if(Person.findByMainEmail("mickael.lecabellec@booleanworks.com") == null)
        {
            Person me = new Person(
                firstName:"Mickael",
                lastName:"LE CABELLEC",
                fullName:"Mickael LE CABELLEC",
                mainEmail:"mickael.lecabellec@booleanworks.com",
                shortDescription:"Project manager")
            
            me.save(flush:true,failOnError:true)
            
            AppUser superadmin = AppUser.findByUsername("superadmin")
            superadmin.person = me
            superadmin.save(flush:true,failOnError:true)
            
  
        }
        
        Activity activity1 = new Activity(
            shortDescription:"Bootstrap ring1 at " + new Date().toGMTString(),
            longDescription:"RAS",
            owner :Person.findByMainEmail("mickael.lecabellec@booleanworks.com"),
            realWorkHours:0.1)
            
        activity1.save(flush:true,failOnError:true)        
        
        Tag.findOrSaveWhere(defaultLabel:"Finished",labelCode:"tag.finished")
        Tag.findOrSaveWhere(defaultLabel:"Started",labelCode:"tag.started")
        Tag.findOrSaveWhere(defaultLabel:"TODO",labelCode:"tag.todo")
        Tag.findOrSaveWhere(defaultLabel:"New",labelCode:"tag.new")
        Tag.findOrSaveWhere(defaultLabel:"WIP",labelCode:"tag.WIP")
       
        
        Item.findOrSaveWhere(shortDescription:"Ring #1",longDescription:"This piece of software")
        
        if(Environment.getCurrent() == Environment.DEVELOPMENT)
        {
           
            Item.findOrSaveWhere(shortDescription:"TARDIS",longDescription:"The TARDIS[nb 1][1] (/ˈtɑːdɪs/; Time and Relative Dimension in Space)[nb 2] is a time machine and spacecraft")
               
            Organization.findOrSaveWhere(shortDescription:"MI-6",longDescription:"For your eyes only.")
        
            Tag.findOrSaveWhere(defaultLabel:"Killed",labelCode:"tag.test.killed")
            Tag.findOrSaveWhere(defaultLabel:"Bastard",labelCode:"tag.test.bastard")
            Tag.findOrSaveWhere(defaultLabel:"Gentleman",labelCode:"tag.test.gentleman")
        
            
            Person.findOrSaveWhere(
                firstName:"John",
                lastName:"DOE",
                fullName:"John DOE",
                mainEmail:"j.doe@booleanworks.com",
                shortDescription:"Famous anonymous")
            
            Person.findOrSaveWhere(
                firstName:"Vladimir",
                lastName:"POUTINE",
                fullName:"Vladimir Vladimirovitch Poutine",
                mainEmail:"vlad-the-red@kickmyass.com",
                shortDescription:"Bloody tsar")  
        
            Person.findOrSaveWhere(
                firstName:"James",
                lastName:"BOND",
                fullName:"BOND, James BOND",
                mainEmail:"007@mi6.uk",
                shortDescription:"007") 
        
            Person.findOrSaveWhere(
                firstName:"George",
                lastName:"CLOONEY",
                fullName:"George Timothy Clooney",
                mainEmail:"gc@nespresso.com",
                shortDescription:"What else ?")   
        
        }
        
        
     
        
    }
    def destroy = {
    }
}
