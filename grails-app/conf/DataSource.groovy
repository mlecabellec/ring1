dataSource {
    //pooled = true
    //driverClassName = "com.mysql.jdbc.Driver"
    //username = "ring1"
    //password = "ring1"
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "ring1"
    password = "ring1"
}


grails {
    mongo {
        host = "ds029630.mongolab.com"
        port = 29630
        username = "ring1"
        password = "ring1"
        databaseName = "ring1"      
    }
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
    //    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            //url = "jdbc:mysql://192.168.56.165:3306/ring1"
            url = "jdbc:h2:ring1_dev;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            //url = "jdbc:mysql://192.168.56.165:3306/ring1"
            url = "jdbc:h2:ring1_test;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            //url = "jdbc:mysql://192.168.56.165:3306/ring1"
            url = "jdbc:h2:ring1_prod;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=false
                validationQuery="SELECT 1"
                jdbcInterceptors="ConnectionState"
            }
        }
    }
}
