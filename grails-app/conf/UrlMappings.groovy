class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "/backoffice"(view:"/backoffice")
        "500"(view:'/error')
	}
}
