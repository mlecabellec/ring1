package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class FileChunkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond FileChunk.list(params), model:[fileChunkInstanceCount: FileChunk.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(FileChunk fileChunkInstance) {
        respond fileChunkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new FileChunk(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(FileChunk fileChunkInstance) {
        if (fileChunkInstance == null) {
            notFound()
            return
        }

        if (fileChunkInstance.hasErrors()) {
            respond fileChunkInstance.errors, view:'create'
            return
        }

        fileChunkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'fileChunkInstance.label', default: 'FileChunk'), fileChunkInstance.id])
                redirect fileChunkInstance
            }
            '*' { respond fileChunkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(FileChunk fileChunkInstance) {
        respond fileChunkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(FileChunk fileChunkInstance) {
        if (fileChunkInstance == null) {
            notFound()
            return
        }

        if (fileChunkInstance.hasErrors()) {
            respond fileChunkInstance.errors, view:'edit'
            return
        }

        fileChunkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'FileChunk.label', default: 'FileChunk'), fileChunkInstance.id])
                redirect fileChunkInstance
            }
            '*'{ respond fileChunkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(FileChunk fileChunkInstance) {

        if (fileChunkInstance == null) {
            notFound()
            return
        }

        fileChunkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'FileChunk.label', default: 'FileChunk'), fileChunkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'fileChunkInstance.label', default: 'FileChunk'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
