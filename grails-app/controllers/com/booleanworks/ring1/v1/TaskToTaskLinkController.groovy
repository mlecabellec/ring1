package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class TaskToTaskLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TaskToTaskLink.list(params), model:[taskToTaskLinkInstanceCount: TaskToTaskLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(TaskToTaskLink taskToTaskLinkInstance) {
        respond taskToTaskLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new TaskToTaskLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(TaskToTaskLink taskToTaskLinkInstance) {
        if (taskToTaskLinkInstance == null) {
            notFound()
            return
        }

        if (taskToTaskLinkInstance.hasErrors()) {
            respond taskToTaskLinkInstance.errors, view:'create'
            return
        }

        taskToTaskLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'taskToTaskLinkInstance.label', default: 'TaskToTaskLink'), taskToTaskLinkInstance.id])
                redirect taskToTaskLinkInstance
            }
            '*' { respond taskToTaskLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(TaskToTaskLink taskToTaskLinkInstance) {
        respond taskToTaskLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(TaskToTaskLink taskToTaskLinkInstance) {
        if (taskToTaskLinkInstance == null) {
            notFound()
            return
        }

        if (taskToTaskLinkInstance.hasErrors()) {
            respond taskToTaskLinkInstance.errors, view:'edit'
            return
        }

        taskToTaskLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TaskToTaskLink.label', default: 'TaskToTaskLink'), taskToTaskLinkInstance.id])
                redirect taskToTaskLinkInstance
            }
            '*'{ respond taskToTaskLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(TaskToTaskLink taskToTaskLinkInstance) {

        if (taskToTaskLinkInstance == null) {
            notFound()
            return
        }

        taskToTaskLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TaskToTaskLink.label', default: 'TaskToTaskLink'), taskToTaskLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'taskToTaskLinkInstance.label', default: 'TaskToTaskLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
