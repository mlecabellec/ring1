package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class OrgToOrgLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond OrgToOrgLink.list(params), model:[orgToOrgLinkInstanceCount: OrgToOrgLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(OrgToOrgLink orgToOrgLinkInstance) {
        respond orgToOrgLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new OrgToOrgLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(OrgToOrgLink orgToOrgLinkInstance) {
        if (orgToOrgLinkInstance == null) {
            notFound()
            return
        }

        if (orgToOrgLinkInstance.hasErrors()) {
            respond orgToOrgLinkInstance.errors, view:'create'
            return
        }

        orgToOrgLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'orgToOrgLinkInstance.label', default: 'OrgToOrgLink'), orgToOrgLinkInstance.id])
                redirect orgToOrgLinkInstance
            }
            '*' { respond orgToOrgLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(OrgToOrgLink orgToOrgLinkInstance) {
        respond orgToOrgLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(OrgToOrgLink orgToOrgLinkInstance) {
        if (orgToOrgLinkInstance == null) {
            notFound()
            return
        }

        if (orgToOrgLinkInstance.hasErrors()) {
            respond orgToOrgLinkInstance.errors, view:'edit'
            return
        }

        orgToOrgLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'OrgToOrgLink.label', default: 'OrgToOrgLink'), orgToOrgLinkInstance.id])
                redirect orgToOrgLinkInstance
            }
            '*'{ respond orgToOrgLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(OrgToOrgLink orgToOrgLinkInstance) {

        if (orgToOrgLinkInstance == null) {
            notFound()
            return
        }

        orgToOrgLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'OrgToOrgLink.label', default: 'OrgToOrgLink'), orgToOrgLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'orgToOrgLinkInstance.label', default: 'OrgToOrgLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
