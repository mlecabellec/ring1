package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class TagToItemLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TagToItemLink.list(params), model:[tagToItemLinkInstanceCount: TagToItemLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(TagToItemLink tagToItemLinkInstance) {
        respond tagToItemLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new TagToItemLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(TagToItemLink tagToItemLinkInstance) {
        if (tagToItemLinkInstance == null) {
            notFound()
            return
        }

        if (tagToItemLinkInstance.hasErrors()) {
            respond tagToItemLinkInstance.errors, view:'create'
            return
        }

        tagToItemLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tagToItemLinkInstance.label', default: 'TagToItemLink'), tagToItemLinkInstance.id])
                redirect tagToItemLinkInstance
            }
            '*' { respond tagToItemLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(TagToItemLink tagToItemLinkInstance) {
        respond tagToItemLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(TagToItemLink tagToItemLinkInstance) {
        if (tagToItemLinkInstance == null) {
            notFound()
            return
        }

        if (tagToItemLinkInstance.hasErrors()) {
            respond tagToItemLinkInstance.errors, view:'edit'
            return
        }

        tagToItemLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TagToItemLink.label', default: 'TagToItemLink'), tagToItemLinkInstance.id])
                redirect tagToItemLinkInstance
            }
            '*'{ respond tagToItemLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(TagToItemLink tagToItemLinkInstance) {

        if (tagToItemLinkInstance == null) {
            notFound()
            return
        }

        tagToItemLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TagToItemLink.label', default: 'TagToItemLink'), tagToItemLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tagToItemLinkInstance.label', default: 'TagToItemLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
