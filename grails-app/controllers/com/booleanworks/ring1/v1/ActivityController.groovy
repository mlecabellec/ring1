package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class ActivityController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Activity.list(params), model:[activityInstanceCount: Activity.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(Activity activityInstance) {
        respond activityInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new Activity(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(Activity activityInstance) {
        if (activityInstance == null) {
            notFound()
            return
        }

        if (activityInstance.hasErrors()) {
            respond activityInstance.errors, view:'create'
            return
        }

        activityInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'activityInstance.label', default: 'Activity'), activityInstance.id])
                redirect activityInstance
            }
            '*' { respond activityInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(Activity activityInstance) {
        respond activityInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(Activity activityInstance) {
        if (activityInstance == null) {
            notFound()
            return
        }

        if (activityInstance.hasErrors()) {
            respond activityInstance.errors, view:'edit'
            return
        }

        activityInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Activity.label', default: 'Activity'), activityInstance.id])
                redirect activityInstance
            }
            '*'{ respond activityInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(Activity activityInstance) {

        if (activityInstance == null) {
            notFound()
            return
        }

        activityInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Activity.label', default: 'Activity'), activityInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'activityInstance.label', default: 'Activity'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
