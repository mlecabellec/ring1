package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class AppRequestMapController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AppRequestMap.list(params), model:[appRequestMapInstanceCount: AppRequestMap.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(AppRequestMap appRequestMapInstance) {
        respond appRequestMapInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new AppRequestMap(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(AppRequestMap appRequestMapInstance) {
        if (appRequestMapInstance == null) {
            notFound()
            return
        }

        if (appRequestMapInstance.hasErrors()) {
            respond appRequestMapInstance.errors, view:'create'
            return
        }

        appRequestMapInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'appRequestMapInstance.label', default: 'AppRequestMap'), appRequestMapInstance.id])
                redirect appRequestMapInstance
            }
            '*' { respond appRequestMapInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(AppRequestMap appRequestMapInstance) {
        respond appRequestMapInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(AppRequestMap appRequestMapInstance) {
        if (appRequestMapInstance == null) {
            notFound()
            return
        }

        if (appRequestMapInstance.hasErrors()) {
            respond appRequestMapInstance.errors, view:'edit'
            return
        }

        appRequestMapInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AppRequestMap.label', default: 'AppRequestMap'), appRequestMapInstance.id])
                redirect appRequestMapInstance
            }
            '*'{ respond appRequestMapInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(AppRequestMap appRequestMapInstance) {

        if (appRequestMapInstance == null) {
            notFound()
            return
        }

        appRequestMapInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AppRequestMap.label', default: 'AppRequestMap'), appRequestMapInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'appRequestMapInstance.label', default: 'AppRequestMap'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
