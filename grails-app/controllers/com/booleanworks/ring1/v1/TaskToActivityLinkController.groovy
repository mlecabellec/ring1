package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class TaskToActivityLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TaskToActivityLink.list(params), model:[taskToActivityLinkInstanceCount: TaskToActivityLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(TaskToActivityLink taskToActivityLinkInstance) {
        respond taskToActivityLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new TaskToActivityLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(TaskToActivityLink taskToActivityLinkInstance) {
        if (taskToActivityLinkInstance == null) {
            notFound()
            return
        }

        if (taskToActivityLinkInstance.hasErrors()) {
            respond taskToActivityLinkInstance.errors, view:'create'
            return
        }

        taskToActivityLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'taskToActivityLinkInstance.label', default: 'TaskToActivityLink'), taskToActivityLinkInstance.id])
                redirect taskToActivityLinkInstance
            }
            '*' { respond taskToActivityLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(TaskToActivityLink taskToActivityLinkInstance) {
        respond taskToActivityLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(TaskToActivityLink taskToActivityLinkInstance) {
        if (taskToActivityLinkInstance == null) {
            notFound()
            return
        }

        if (taskToActivityLinkInstance.hasErrors()) {
            respond taskToActivityLinkInstance.errors, view:'edit'
            return
        }

        taskToActivityLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TaskToActivityLink.label', default: 'TaskToActivityLink'), taskToActivityLinkInstance.id])
                redirect taskToActivityLinkInstance
            }
            '*'{ respond taskToActivityLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(TaskToActivityLink taskToActivityLinkInstance) {

        if (taskToActivityLinkInstance == null) {
            notFound()
            return
        }

        taskToActivityLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TaskToActivityLink.label', default: 'TaskToActivityLink'), taskToActivityLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'taskToActivityLinkInstance.label', default: 'TaskToActivityLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
