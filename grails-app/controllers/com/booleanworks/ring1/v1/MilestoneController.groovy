package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class MilestoneController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Milestone.list(params), model:[milestoneInstanceCount: Milestone.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(Milestone milestoneInstance) {
        respond milestoneInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new Milestone(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(Milestone milestoneInstance) {
        if (milestoneInstance == null) {
            notFound()
            return
        }

        if (milestoneInstance.hasErrors()) {
            respond milestoneInstance.errors, view:'create'
            return
        }

        milestoneInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'milestoneInstance.label', default: 'Milestone'), milestoneInstance.id])
                redirect milestoneInstance
            }
            '*' { respond milestoneInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(Milestone milestoneInstance) {
        respond milestoneInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(Milestone milestoneInstance) {
        if (milestoneInstance == null) {
            notFound()
            return
        }

        if (milestoneInstance.hasErrors()) {
            respond milestoneInstance.errors, view:'edit'
            return
        }

        milestoneInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Milestone.label', default: 'Milestone'), milestoneInstance.id])
                redirect milestoneInstance
            }
            '*'{ respond milestoneInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(Milestone milestoneInstance) {

        if (milestoneInstance == null) {
            notFound()
            return
        }

        milestoneInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Milestone.label', default: 'Milestone'), milestoneInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'milestoneInstance.label', default: 'Milestone'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
