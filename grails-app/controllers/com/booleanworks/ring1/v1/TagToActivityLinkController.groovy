package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class TagToActivityLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TagToActivityLink.list(params), model:[tagToActivityLinkInstanceCount: TagToActivityLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(TagToActivityLink tagToActivityLinkInstance) {
        respond tagToActivityLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new TagToActivityLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(TagToActivityLink tagToActivityLinkInstance) {
        if (tagToActivityLinkInstance == null) {
            notFound()
            return
        }

        if (tagToActivityLinkInstance.hasErrors()) {
            respond tagToActivityLinkInstance.errors, view:'create'
            return
        }

        tagToActivityLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tagToActivityLinkInstance.label', default: 'TagToActivityLink'), tagToActivityLinkInstance.id])
                redirect tagToActivityLinkInstance
            }
            '*' { respond tagToActivityLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(TagToActivityLink tagToActivityLinkInstance) {
        respond tagToActivityLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(TagToActivityLink tagToActivityLinkInstance) {
        if (tagToActivityLinkInstance == null) {
            notFound()
            return
        }

        if (tagToActivityLinkInstance.hasErrors()) {
            respond tagToActivityLinkInstance.errors, view:'edit'
            return
        }

        tagToActivityLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TagToActivityLink.label', default: 'TagToActivityLink'), tagToActivityLinkInstance.id])
                redirect tagToActivityLinkInstance
            }
            '*'{ respond tagToActivityLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(TagToActivityLink tagToActivityLinkInstance) {

        if (tagToActivityLinkInstance == null) {
            notFound()
            return
        }

        tagToActivityLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TagToActivityLink.label', default: 'TagToActivityLink'), tagToActivityLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tagToActivityLinkInstance.label', default: 'TagToActivityLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
