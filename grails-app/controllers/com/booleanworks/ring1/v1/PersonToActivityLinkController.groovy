package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class PersonToActivityLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PersonToActivityLink.list(params), model:[personToActivityLinkInstanceCount: PersonToActivityLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(PersonToActivityLink personToActivityLinkInstance) {
        respond personToActivityLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new PersonToActivityLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(PersonToActivityLink personToActivityLinkInstance) {
        if (personToActivityLinkInstance == null) {
            notFound()
            return
        }

        if (personToActivityLinkInstance.hasErrors()) {
            respond personToActivityLinkInstance.errors, view:'create'
            return
        }

        personToActivityLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'personToActivityLinkInstance.label', default: 'PersonToActivityLink'), personToActivityLinkInstance.id])
                redirect personToActivityLinkInstance
            }
            '*' { respond personToActivityLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(PersonToActivityLink personToActivityLinkInstance) {
        respond personToActivityLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(PersonToActivityLink personToActivityLinkInstance) {
        if (personToActivityLinkInstance == null) {
            notFound()
            return
        }

        if (personToActivityLinkInstance.hasErrors()) {
            respond personToActivityLinkInstance.errors, view:'edit'
            return
        }

        personToActivityLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PersonToActivityLink.label', default: 'PersonToActivityLink'), personToActivityLinkInstance.id])
                redirect personToActivityLinkInstance
            }
            '*'{ respond personToActivityLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(PersonToActivityLink personToActivityLinkInstance) {

        if (personToActivityLinkInstance == null) {
            notFound()
            return
        }

        personToActivityLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PersonToActivityLink.label', default: 'PersonToActivityLink'), personToActivityLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'personToActivityLinkInstance.label', default: 'PersonToActivityLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
