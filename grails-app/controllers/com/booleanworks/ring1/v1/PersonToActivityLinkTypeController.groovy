package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class PersonToActivityLinkTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PersonToActivityLinkType.list(params), model:[personToActivityLinkTypeInstanceCount: PersonToActivityLinkType.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(PersonToActivityLinkType personToActivityLinkTypeInstance) {
        respond personToActivityLinkTypeInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new PersonToActivityLinkType(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(PersonToActivityLinkType personToActivityLinkTypeInstance) {
        if (personToActivityLinkTypeInstance == null) {
            notFound()
            return
        }

        if (personToActivityLinkTypeInstance.hasErrors()) {
            respond personToActivityLinkTypeInstance.errors, view:'create'
            return
        }

        personToActivityLinkTypeInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'personToActivityLinkTypeInstance.label', default: 'PersonToActivityLinkType'), personToActivityLinkTypeInstance.id])
                redirect personToActivityLinkTypeInstance
            }
            '*' { respond personToActivityLinkTypeInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(PersonToActivityLinkType personToActivityLinkTypeInstance) {
        respond personToActivityLinkTypeInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(PersonToActivityLinkType personToActivityLinkTypeInstance) {
        if (personToActivityLinkTypeInstance == null) {
            notFound()
            return
        }

        if (personToActivityLinkTypeInstance.hasErrors()) {
            respond personToActivityLinkTypeInstance.errors, view:'edit'
            return
        }

        personToActivityLinkTypeInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PersonToActivityLinkType.label', default: 'PersonToActivityLinkType'), personToActivityLinkTypeInstance.id])
                redirect personToActivityLinkTypeInstance
            }
            '*'{ respond personToActivityLinkTypeInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(PersonToActivityLinkType personToActivityLinkTypeInstance) {

        if (personToActivityLinkTypeInstance == null) {
            notFound()
            return
        }

        personToActivityLinkTypeInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PersonToActivityLinkType.label', default: 'PersonToActivityLinkType'), personToActivityLinkTypeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'personToActivityLinkTypeInstance.label', default: 'PersonToActivityLinkType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
