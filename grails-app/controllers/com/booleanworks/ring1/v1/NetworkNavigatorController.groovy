package com.booleanworks.ring1.v1

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

class NetworkNavigatorController {

    def springSecurityService

    def searchableService
    //def elasticSearchService    
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def index() { }
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_init_left_list() {
        
        List<Object> results = [] 
        
        results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "asc")
        //results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "desc")
        
        render(view:"searchresults", model:[results:results])
        
    }   
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_init_right_list() {
        
        List<Object> results = [] 
        
        results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "asc")
        //results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "desc")
        
        render(view:"searchresults", model:[results:results])
        
    }
    
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def search_related_items() {
        
        if(params.q == null || params.q.length() < 2)
        {
            render(status:200,text:"")
            return
        }
        
    
        List<Object> results = [] 
        //List<Object> results = searchableService.search(params?.q,[offset:0,max:10,reload:true]).results
        
        results += Person.search("*" + params?.q + "*",[offset:0,max:4,reload:true]).results
        results += Tag.search("*" + params?.q + "*",[offset:0,max:4,reload:true]).results
        results += Task.search("*" + params?.q + "*",[offset:0,max:4,reload:true]).results
        
        if(results.size() == 0)
        {
            results += searchableService.search("*" + params?.q + "*",[offset:0,max:10,reload:true]).results      
        }
       

        if(results.size() == 0)
        {
            PerodicIndexUpdaterJob.triggerNow()
        }

        
        
        
        System.out.println("params.q = " + params.q) 
        System.out.println("results.size() = " + results.size()) 
        System.out.println("results = " + results) 
        
        render(view:"searchresults", model:[results:results])
             
        
    }

    @Secured(["hasRole('SIMPLE_USER')"])
    def get_json_graph_selfcentered() {
        
        
        def resultGraph = [nodes: [] , links :[]] 
        
        String principal = springSecurityService.getPrincipal().username
        
        log.info("principal: " + principal)
        System.out.println("principal: " + principal) 
        
        if(principal == null)
        {
            render resultGraph as JSON
            return
        }
        
        AppUser appUser = AppUser.findByUsername(principal)
        log.info("appUser.username: " + appUser?.username)
        System.out.println("appUser.username: " + appUser?.username) 

        if(appUser == null)
        {
            render resultGraph as JSON
            return
        }  
        
        Person myself = appUser.person
        //person.refresh()
        
        if(myself == null)
        {
            render(status:403,text:"No Person found. " )
            return
        }    
        log.info("appUser.person: " + appUser?.person)
        System.out.println("appUser.person: " + appUser?.person)        

        HashSet<Person> persons = new HashSet<Person>() 
        HashSet<Activity> activities = new HashSet<Activity>()
        HashSet<Task> tasks = new HashSet<Task>()
        HashSet<Tag> tags = new HashSet<Tag>()
        HashSet<Milestone> milestones = new HashSet<Milestone>()
        HashSet<Item> items = new HashSet<Item>()
            
        HashSet<PersonToActivityLink> personToActivityLinks = new HashSet<PersonToActivityLink>() 
        HashSet<TagToActivityLink> tagToActivityLinks = new HashSet<TagToActivityLink>() 
        HashSet<TaskToActivityLink> taskToActivityLinks = new HashSet<TaskToActivityLink>() 
            
        persons += myself
            
        persons.each{ person ->
            PersonToActivityLink.findAllWhere(person:person).each{ link ->
                personToActivityLinks += link
                activities += link.activity
            }
        }
            
        activities.each{ activity ->
            PersonToActivityLink.findAllWhere(activity:activity).each{ link ->
                personToActivityLinks += link
                persons += link.person
            }
                
            TaskToActivityLink.findAllWhere(activity:activity).each{ link ->
                taskToActivityLinks += link
                tasks += link.task
            }  
                
            TagToActivityLink.findAllWhere(activity:activity).each{ link ->
                tagToActivityLinks += link
                tags += link.tag
            }                
        }            
            
        tasks.each{ task ->
            TaskToActivityLink.findAllWhere(task:task).each{ link ->
                taskToActivityLinks += link
                activities += link.activity
            } 
        }
        
        persons.each{ person ->
            PersonToActivityLink.findAllWhere(person:person).each{ link ->
                personToActivityLinks += link
                activities += link.activity
            }
        }
            
        activities.each{ activity ->
            PersonToActivityLink.findAllWhere(activity:activity).each{ link ->
                personToActivityLinks += link
                persons += link.person
            }
                
            TaskToActivityLink.findAllWhere(activity:activity).each{ link ->
                taskToActivityLinks += link
                tasks += link.task
            }  
                
            TagToActivityLink.findAllWhere(activity:activity).each{ link ->
                tagToActivityLinks += link
                tags += link.tag
            }                
        }            
            
        tasks.each{ task ->
            TaskToActivityLink.findAllWhere(task:task).each{ link ->
                taskToActivityLinks += link
                activities += link.activity
            } 
        }        
            
        HashSet<Object> entityNodesHashSet = new HashSet<Object>()
        persons.each{
            entityNodesHashSet += it
        }
        activities.each{
            entityNodesHashSet += it
        }
        tasks.each{
            entityNodesHashSet += it
        }
        tags.each{
            entityNodesHashSet += it
        }            
        ArrayList<Object> entityNodesArrayList = new ArrayList<Object>(entityNodesHashSet)
        entityNodesArrayList.each{
            resultGraph.nodes += [node_data:it, node_class: it.getClass().getSimpleName(), node_id: it.id, node_text: it.toString()]
        }
            
        personToActivityLinks.each{
            resultGraph.links += [link_data:it, source:entityNodesArrayList.indexOf(it.person), target:entityNodesArrayList.indexOf(it.activity), link_id:it.id]
        }
        tagToActivityLinks.each{
            resultGraph.links += [link_data:it, source:entityNodesArrayList.indexOf(it.tag), target:entityNodesArrayList.indexOf(it.activity), link_id:it.id]
        }
        taskToActivityLinks.each{
            resultGraph.links += [link_data:it, source:entityNodesArrayList.indexOf(it.task), target:entityNodesArrayList.indexOf(it.activity), link_id:it.id]
        }            
        
        
        render resultGraph as JSON
        
    }     
    
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_json_graph_from_double_request() {
        
        
        def resultGraph = [nodes: [] , links :[]] 
        
        
        if(params.leftString != null && params.rightString != null)
        {
            HashSet<Person> persons = new HashSet<Person>() 
            HashSet<Activity> activities = new HashSet<Activity>()
            HashSet<Task> tasks = new HashSet<Task>()
            HashSet<Tag> tags = new HashSet<Tag>()
            HashSet<Milestone> milestones = new HashSet<Milestone>()
            HashSet<Item> items = new HashSet<Item>()
            
            HashSet<PersonToActivityLink> personToActivityLinks = new HashSet<PersonToActivityLink>() 
            HashSet<TagToActivityLink> tagToActivityLinks = new HashSet<TagToActivityLink>() 
            HashSet<TaskToActivityLink> taskToActivityLinks = new HashSet<TaskToActivityLink>() 
            
            persons += Person.search(params.leftString,[offset:0,max:10,reload:true]).results
            persons += Person.search(params.rightString,[offset:0,max:10,reload:true]).results
            activities += Activity.search(params.leftString,[offset:0,max:10,reload:true]).results
            activities += Activity.search(params.rightString,[offset:0,max:10,reload:true]).results
            tasks += Task.search(params.leftString,[offset:0,max:10,reload:true]).results
            tasks += Task.search(params.rightString,[offset:0,max:10,reload:true]).results
            tags += Tag.search(params.leftString,[offset:0,max:10,reload:true]).results
            tags += Tag.search(params.rightString,[offset:0,max:10,reload:true]).results
            
            persons.each{ person ->
                PersonToActivityLink.findAllWhere(person:person).each{ link ->
                    personToActivityLinks += link
                    activities += link.activity
                }
            }
            
            activities.each{ activity ->
                PersonToActivityLink.findAllWhere(activity:activity).each{ link ->
                    personToActivityLinks += link
                    persons += link.person
                }
                
                TaskToActivityLink.findAllWhere(activity:activity).each{ link ->
                    taskToActivityLinks += link
                    tasks += link.task
                }  
                
                TagToActivityLink.findAllWhere(activity:activity).each{ link ->
                    tagToActivityLinks += link
                    tags += link.tag
                }                
            }            
            
            tasks.each{ task ->
                TaskToActivityLink.findAllWhere(task:task).each{ link ->
                    taskToActivityLinks += link
                    activities += link.activity
                } 
            }
            
            persons.each{ person ->
                PersonToActivityLink.findAllWhere(person:person).each{ link ->
                    personToActivityLinks += link
                    activities += link.activity
                }
            }
            
            activities.each{ activity ->
                PersonToActivityLink.findAllWhere(activity:activity).each{ link ->
                    personToActivityLinks += link
                    persons += link.person
                }
                
                TaskToActivityLink.findAllWhere(activity:activity).each{ link ->
                    taskToActivityLinks += link
                    tasks += link.task
                }  
                
                TagToActivityLink.findAllWhere(activity:activity).each{ link ->
                    tagToActivityLinks += link
                    tags += link.tag
                }                
            }            
            
            tasks.each{ task ->
                TaskToActivityLink.findAllWhere(task:task).each{ link ->
                    taskToActivityLinks += link
                    activities += link.activity
                } 
            }            
            
            HashSet<Object> entityNodesHashSet = new HashSet<Object>()
            persons.each{
                entityNodesHashSet += it
            }
            activities.each{
                entityNodesHashSet += it
            }
            tasks.each{
                entityNodesHashSet += it
            }
            tags.each{
                entityNodesHashSet += it
            }            
            ArrayList<Object> entityNodesArrayList = new ArrayList<Object>(entityNodesHashSet)
            entityNodesArrayList.each{
                resultGraph.nodes += [node_data:it, node_class: it.getClass().getSimpleName(), node_id: it.id, node_text: it.toString()]
            }
            
            personToActivityLinks.each{
                resultGraph.links += [link_data:it, source:entityNodesArrayList.indexOf(it.person), target:entityNodesArrayList.indexOf(it.activity), link_id:it.id]
            }
            tagToActivityLinks.each{
                resultGraph.links += [link_data:it, source:entityNodesArrayList.indexOf(it.tag), target:entityNodesArrayList.indexOf(it.activity), link_id:it.id]
            }
            taskToActivityLinks.each{
                resultGraph.links += [link_data:it, source:entityNodesArrayList.indexOf(it.task), target:entityNodesArrayList.indexOf(it.activity), link_id:it.id]
            }            
        }
        
        render resultGraph as JSON
        
    }    
    
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_search_result_person() {
        if( params.id != null && Person.get(params.id) != null) {
            render(view:"searchresult-person", model:[person:Person.get(params.id)])
        }
        else
        {
            render(status:520,text:"Person not found !")
        }
    }  
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_search_result_tag() {
        if( params.id != null && Tag.get(params.id) != null) {
            render(view:"searchresult-tag", model:[tag:Tag.get(params.id)])
        }
        else
        {
            render(status:520,text:"Tag not found !")
        }
    } 
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_search_result_task() {
        if( params.id != null && Task.get(params.id) != null) {
            render(view:"searchresult-task", model:[task:Task.get(params.id)])
        }
        else
        {
            render(status:520,text:"Task not found !")
        }
    }     
    
}
