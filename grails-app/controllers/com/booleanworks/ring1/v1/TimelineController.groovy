package com.booleanworks.ring1.v1

import grails.plugin.springsecurity.annotation.Secured

class TimelineController {
    
    def springSecurityService

    def searchableService
    //def elasticSearchService

    @Secured(["hasRole('SIMPLE_USER')"])
    def index() { }
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def index_1() { }
    
   
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_single_activity() {
    
        if(params?.id == null)
        {
            
            render(status:404,text:"No ID provided")
            return
        }
        
        
        
        Activity activityInstance = Activity.get(params?.id)
        
        if(activityInstance == null)
        {
            render(status:404,text:"No object matching the ID")
            return
        } else
        {
            render(view:"activity-item",model:[activityInstance:activityInstance])
        }

        
        
        
        
    }

    @Secured(["hasRole('SIMPLE_USER')"])
    def get_my_activity_timeline() { 
    
        
        String principal = springSecurityService.getPrincipal().username
        
        log.info("principal: " + principal)
        System.out.println("principal: " + principal) 
        
        if(principal == null)
        {
            render(status:403,text:"No principal found.")
            return
        }
        
        AppUser appUser = AppUser.findByUsername(principal)
        log.info("appUser.username: " + appUser?.username)
        System.out.println("appUser.username: " + appUser?.username) 

        if(appUser == null)
        {
            render(status:403,text:"No AppUser found.")
            return
        }  
        
        Person person = appUser.person
        //person.refresh()
        
        if(person == null)
        {
            render(status:403,text:"No Person found. " )
            return
        }    
        log.info("appUser.person: " + appUser?.person)
        System.out.println("appUser.person: " + appUser?.person) 
        
        List<Activity> activityList = Activity.findAllByOwner(person,[offset:0,max:10,sort:"dateCreated",order:"desc"])
    
        render(view:"activity-timeline",model:[activityList:activityList])
        
        
    }
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def create_or_update_new_activity() {
            
        System.out.println("params: " + params) 
        
        String principal = springSecurityService.getPrincipal().username
        
        log.info("principal: " + principal)
        System.out.println("principal: " + principal) 
        
        if(principal == null)
        {
            render(status:403,text:"No principal found.")
            return
        }
        
        AppUser appUser = AppUser.findByUsername(principal)
        log.info("appUser.username: " + appUser?.username)
        System.out.println("appUser.username: " + appUser?.username) 

        if(appUser == null)
        {
            render(status:403,text:"No AppUser found.")
            return
        }  
        
        Person person = appUser.person
        //person.refresh()
        
        if(person == null)
        {
            render(status:403,text:"No Person found. " )
            return
        }    
        log.info("appUser.person: " + appUser?.person)
        System.out.println("appUser.person: " + appUser?.person)  
        
        
        if( params.mode == "update" &&  params.id != null && Activity.get(params.id) != null)
        {
            render(status:200,text:"<li>TEST CAS #1 </li>") 
 
        }else
        {
            Activity newActivity = new Activity(
                shortDescription:params.shortDescription,
                longDescription:params.longDescription,
                owner:person, 
                realWorkHours:params.workHours)
            
            if(!newActivity.validate(["shortDescription"]))
            {
                newActivity.discard()
                render(status:520,text:"shortDescription not accepted")
                return
            }
            if(!newActivity.validate(["longDescription"]))
            {
                newActivity.discard()
                render(status:520,text:"longDescription not accepted")
                return
            }
            if(!newActivity.validate(["owner"]))
            {
                newActivity.discard()
                render(status:520,text:"owner not accepted")
                return
            }
            if(!newActivity.validate(["realWorkHours"]))
            {
                newActivity.discard()
                render(status:520,text:"realWorkHours not accepted")
                return
            }
            if(!newActivity.validate())
            {
                newActivity.discard()
                render(status:520,text:"activity object not accepted (for unknwon reason")
                return
            }

            newActivity = newActivity.save(flush:true,failOnError:true)
            render(view:"activity-item",model:[activityInstance:newActivity])
            

            
        }

    }
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def link_to_activity() {
            
        System.out.println("params: " + params) 
        
        String principal = springSecurityService.getPrincipal().username
        
        log.info("principal: " + principal)
        System.out.println("principal: " + principal) 
        
        if(principal == null)
        {
            render(status:403,text:"No principal found.")
            return
        }
        
        AppUser appUser = AppUser.findByUsername(principal)
        log.info("appUser.username: " + appUser?.username)
        System.out.println("appUser.username: " + appUser?.username) 

        if(appUser == null)
        {
            render(status:403,text:"No AppUser found.")
            return
        }  
        
        Person person = appUser.person
        //person.refresh()
        
        if(person == null)
        {
            render(status:403,text:"No Person found. " )
            return
        }    
        log.info("appUser.person: " + appUser?.person)
        System.out.println("appUser.person: " + appUser?.person)  
        
        
        //TODO: Eh ! finish it !!!
        
        String activity_id = params?.activity_id
        String linked_id = params?.linked_id
        
        if(activity_id == null || linked_id == null)
        {
            render(status:520,text:"NULL parameters provided")
            return
        }
        
        if(!activity_id.matches("activity_[0-9]*") || !linked_id.matches("[a-z]*_[0-9]*"))
        {
            render(status:520,text:"Incorrect parameters provided")
            return
            
        }
        
        Activity targetActivity = Activity.get(activity_id.replaceFirst("activity_",""))
        
        if(targetActivity == null)
        {
            render(status:520,text:"No matching activity found")
            return
            
        }
        
        if(linked_id.matches("tag_[0-9]*"))
        {
            Tag sourceTag = Tag.get(linked_id.replaceFirst("tag_",""))
            if(sourceTag == null)
            {
                targetActivity.discard()
                render(status:520,text:"No matching tag found")
                return
                
            }else
            {
                TagToActivityLink.findOrSaveWhere(activity:targetActivity,tag:sourceTag)
            }
 
        }

        if(linked_id.matches("person_[0-9]*"))
        {
            Person sourcePerson = Person.get(linked_id.replaceFirst("person_",""))
            if(sourcePerson == null)
            {
                targetActivity.discard()
                render(status:520,text:"No matching person found")
                return
                
            }else
            {
                PersonToActivityLink.findOrSaveWhere(activity:targetActivity,person:sourcePerson)
            }
 
        }
        
        if(linked_id.matches("task_[0-9]*"))
        {
            Task sourceTask = Task.get(linked_id.replaceFirst("task_",""))
            if(sourceTask == null)
            {
                targetActivity.discard()
                render(status:520,text:"No matching task found")
                return
                
            }else
            {
                TaskToActivityLink.findOrSaveWhere(activity:targetActivity,task:sourceTask)
            }
 
        }        
        
        
        render(view:"activity-item",model:[activityInstance:targetActivity])
                

    }    
    
    
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_init_left_list() {
        
        List<Object> results = [] 
        
        results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "asc")
        //results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "desc")
        
        render(view:"searchresults", model:[results:results])
        
    }   
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_init_right_list() {
        
        List<Object> results = [] 
        
        results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "asc")
        //results += Tag.list(max: 5, offset: 0, sort: "dateCreated", order: "desc")
        
        render(view:"searchresults", model:[results:results])
        
    }
    
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def search_related_items() {
        
        if(params.q == null || params.q.length() < 2)
        {
            render(status:200,text:"")
            return
        }
        
    
        List<Object> results = [] 
        //List<Object> results = searchableService.search(params?.q,[offset:0,max:10,reload:true]).results
        
        results += Person.search("*" + params?.q + "*",[offset:0,max:4,reload:true]).results
        results += Tag.search("*" + params?.q + "*",[offset:0,max:4,reload:true]).results
        results += Task.search("*" + params?.q + "*",[offset:0,max:4,reload:true]).results
        
        if(results.size() == 0)
        {
            results += searchableService.search("*" + params?.q + "*",[offset:0,max:10,reload:true]).results      
        }
       

        if(results.size() == 0)
        {
            PerodicIndexUpdaterJob.triggerNow()
        }

        
        
        
        System.out.println("params.q = " + params.q) 
        System.out.println("results.size() = " + results.size()) 
        System.out.println("results = " + results) 
        
        render(view:"searchresults", model:[results:results])
             
        
    }
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_search_result_person() {
        if( params.id != null && Person.get(params.id) != null) {
            render(view:"searchresult-person", model:[person:Person.get(params.id)])
        }
        else
        {
            render(status:520,text:"Person not found !")
        }
    }  
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_search_result_tag() {
        if( params.id != null && Tag.get(params.id) != null) {
            render(view:"searchresult-tag", model:[tag:Tag.get(params.id)])
        }
        else
        {
            render(status:520,text:"Tag not found !")
        }
    } 
    
    @Secured(["hasRole('SIMPLE_USER')"])
    def get_search_result_task() {
        if( params.id != null && Task.get(params.id) != null) {
            render(view:"searchresult-task", model:[task:Task.get(params.id)])
        }
        else
        {
            render(status:520,text:"Task not found !")
        }
    } 
    
    
}
