package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class AppUserAppRoleController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AppUserAppRole.list(params), model:[appUserAppRoleInstanceCount: AppUserAppRole.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(AppUserAppRole appUserAppRoleInstance) {
        respond appUserAppRoleInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new AppUserAppRole(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(AppUserAppRole appUserAppRoleInstance) {
        if (appUserAppRoleInstance == null) {
            notFound()
            return
        }

        if (appUserAppRoleInstance.hasErrors()) {
            respond appUserAppRoleInstance.errors, view:'create'
            return
        }

        appUserAppRoleInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'appUserAppRoleInstance.label', default: 'AppUserAppRole'), appUserAppRoleInstance.id])
                redirect appUserAppRoleInstance
            }
            '*' { respond appUserAppRoleInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(AppUserAppRole appUserAppRoleInstance) {
        respond appUserAppRoleInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(AppUserAppRole appUserAppRoleInstance) {
        if (appUserAppRoleInstance == null) {
            notFound()
            return
        }

        if (appUserAppRoleInstance.hasErrors()) {
            respond appUserAppRoleInstance.errors, view:'edit'
            return
        }

        appUserAppRoleInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AppUserAppRole.label', default: 'AppUserAppRole'), appUserAppRoleInstance.id])
                redirect appUserAppRoleInstance
            }
            '*'{ respond appUserAppRoleInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(AppUserAppRole appUserAppRoleInstance) {

        if (appUserAppRoleInstance == null) {
            notFound()
            return
        }

        appUserAppRoleInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AppUserAppRole.label', default: 'AppUserAppRole'), appUserAppRoleInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUserAppRoleInstance.label', default: 'AppUserAppRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
