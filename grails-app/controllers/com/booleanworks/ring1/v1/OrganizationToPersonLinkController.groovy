package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class OrganizationToPersonLinkController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond OrganizationToPersonLink.list(params), model:[organizationToPersonLinkInstanceCount: OrganizationToPersonLink.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(OrganizationToPersonLink organizationToPersonLinkInstance) {
        respond organizationToPersonLinkInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new OrganizationToPersonLink(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(OrganizationToPersonLink organizationToPersonLinkInstance) {
        if (organizationToPersonLinkInstance == null) {
            notFound()
            return
        }

        if (organizationToPersonLinkInstance.hasErrors()) {
            respond organizationToPersonLinkInstance.errors, view:'create'
            return
        }

        organizationToPersonLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'organizationToPersonLinkInstance.label', default: 'OrganizationToPersonLink'), organizationToPersonLinkInstance.id])
                redirect organizationToPersonLinkInstance
            }
            '*' { respond organizationToPersonLinkInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(OrganizationToPersonLink organizationToPersonLinkInstance) {
        respond organizationToPersonLinkInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(OrganizationToPersonLink organizationToPersonLinkInstance) {
        if (organizationToPersonLinkInstance == null) {
            notFound()
            return
        }

        if (organizationToPersonLinkInstance.hasErrors()) {
            respond organizationToPersonLinkInstance.errors, view:'edit'
            return
        }

        organizationToPersonLinkInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'OrganizationToPersonLink.label', default: 'OrganizationToPersonLink'), organizationToPersonLinkInstance.id])
                redirect organizationToPersonLinkInstance
            }
            '*'{ respond organizationToPersonLinkInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(OrganizationToPersonLink organizationToPersonLinkInstance) {

        if (organizationToPersonLinkInstance == null) {
            notFound()
            return
        }

        organizationToPersonLinkInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'OrganizationToPersonLink.label', default: 'OrganizationToPersonLink'), organizationToPersonLinkInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'organizationToPersonLinkInstance.label', default: 'OrganizationToPersonLink'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
