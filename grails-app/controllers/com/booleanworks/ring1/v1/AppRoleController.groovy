package com.booleanworks.ring1.v1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
class AppRoleController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["hasRole('SUPER_ADMIN')"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AppRole.list(params), model:[appRoleInstanceCount: AppRole.count()]
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def show(AppRole appRoleInstance) {
        respond appRoleInstance
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def create() {
        respond new AppRole(params)
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def save(AppRole appRoleInstance) {
        if (appRoleInstance == null) {
            notFound()
            return
        }

        if (appRoleInstance.hasErrors()) {
            respond appRoleInstance.errors, view:'create'
            return
        }

        appRoleInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'appRoleInstance.label', default: 'AppRole'), appRoleInstance.id])
                redirect appRoleInstance
            }
            '*' { respond appRoleInstance, [status: CREATED] }
        }
    }

    @Secured(["hasRole('SUPER_ADMIN')"])
    def edit(AppRole appRoleInstance) {
        respond appRoleInstance
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def update(AppRole appRoleInstance) {
        if (appRoleInstance == null) {
            notFound()
            return
        }

        if (appRoleInstance.hasErrors()) {
            respond appRoleInstance.errors, view:'edit'
            return
        }

        appRoleInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AppRole.label', default: 'AppRole'), appRoleInstance.id])
                redirect appRoleInstance
            }
            '*'{ respond appRoleInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(["hasRole('SUPER_ADMIN')"])
    def delete(AppRole appRoleInstance) {

        if (appRoleInstance == null) {
            notFound()
            return
        }

        appRoleInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AppRole.label', default: 'AppRole'), appRoleInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'appRoleInstance.label', default: 'AppRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
