package com.booleanworks.ring1.v1

class Task {

    //static mapWith = "mongo"
    static searchable = true
    
    static constraints = {
        shortDescription(nullable:false,blank:false,size:0..200)
        longDescription(nullable:false,blank:true,size:0..4095)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true)
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    String shortDescription
    String longDescription
    
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }    
    
    public String toString()
    {
        return "Task[" + id + "]"
        //return "Task[" + id + "], " + this.properties
    }
}
