package com.booleanworks.ring1.v1

class TaskToTaskLink {

    //static mapWith = "mongo"
    ////static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static belongsTo = [sourceTask : Task, destinationTask : Task]
    
    static constraints = {
        dateCreated(nullable:true)
        lastUpdated (nullable:true) 
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }    
    
    public String toString()
    {
        return "TaskToTaskLink[" + id + "]"
        //return "TaskToTaskLink[" + id + "], " + this.properties
    }
}
