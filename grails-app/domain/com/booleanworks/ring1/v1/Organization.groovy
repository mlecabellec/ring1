package com.booleanworks.ring1.v1

class Organization {

    //static mapWith = "mongo"
    static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static constraints = {
        dateCreated(nullable:true)
        lastUpdated (nullable:true)
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
        
        parentOrganization(nullable:true)
        mainManager(nullable:true)
        deputyManager(nullable:true)
        
        acronym(nullable:true,blank:false,size:0..32)
        
        shortDescription(nullable:false,blank:false,size:0..200)
        longDescription(nullable:false,blank:true,size:0..4095)        
    }
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }     
    
    Organization parentOrganization 
    Person mainManager
    Person deputyManager
    
    String acronym
    
    String shortDescription
    String longDescription    
    
    public String toString()
    {
        return "Organization: " + shortDescription + "[" + id + "]"
    }
}
