package com.booleanworks.ring1.v1

class PersonToActivityLink {

    //static mapWith = "mongo"
    //static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static belongsTo = [ activity : Activity , person : Person]
    
    static constraints = {
        personToActivityLinkType(nullable:true)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true) 
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    PersonToActivityLinkType personToActivityLinkType
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }     
    
    public String toString()
    {
        return "PersonToActivityLink[" + id + "]"
        //return "PersonToActivityLink[" + id + "], " + this.properties
    }
}
