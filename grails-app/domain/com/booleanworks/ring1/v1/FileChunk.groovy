package com.booleanworks.ring1.v1

class FileChunk {

    //static mapWith = "mongo"
    ////static searchable = true
    
    static  belongsTo = [fileItem : FileItem]
    
    
    static mapping = {
        autoTimestamp true
    }    
    
    static constraints = {
        chunkSize(nullable:false)
        chunkIndex(nullable:false)
    
        previousChunk(nullable:true)
        newtChunk(nullable:true)
    
        payload(nullable:false) 
        sha1Hash(nullable:false)
    
        dateCreated(nullable:true)
        lastUpdated (nullable:true)       
    }
    
    Long chunkSize
    Long chunkIndex
    
    FileChunk previousChunk
    FileChunk newtChunk
    
    byte[] payload 
    byte[] sha1Hash
    
    
    Date dateCreated
    Date lastUpdated  
    
    public String toString()
    {
        return "FileChunk[" + id + "]"
        //return "FileChunk[" + id + "], " + this.properties
    }
}
