package com.booleanworks.ring1.v1

import org.springframework.http.HttpMethod

class AppRequestMap {

    //static mapWith = "mongo"
    ////static searchable = true    
    
    String url
    String configAttribute
    HttpMethod httpMethod

    static mapping = {
        cache true
    }

    static constraints = {
        url blank: false, unique: 'httpMethod'
        configAttribute blank: false
        httpMethod nullable: true
    }
}
