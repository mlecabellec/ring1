package com.booleanworks.ring1.v1

class Activity {

    //static mapWith = "mongo"
    static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    
    static hasMany = [tagLinks : TagToActivityLink , personLinks :PersonToActivityLink, taskLinks :TaskToActivityLink]
    
    static constraints = {
        shortDescription(nullable:false,blank:false,size:0..200)
        longDescription(nullable:false,blank:true,size:0..4095)
        
        owner(nullable:false)
        
        realWorkHours(nullable:false,min:0.0d)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true)   
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)
    }
    
    String shortDescription
    String longDescription
    
    Person owner 
    
    Double realWorkHours
        
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    } 
    
}
