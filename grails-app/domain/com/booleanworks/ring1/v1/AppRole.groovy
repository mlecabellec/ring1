package com.booleanworks.ring1.v1

class AppRole {

    //static mapWith = "mongo"
    //static searchable = true  
    
    String authority

    static mapping = {
        cache true
    }

    static constraints = {
        authority blank: false, unique: true
    }
    
    public String toString()
    {
        return authority + " [" + id + "]"
    }    
}
