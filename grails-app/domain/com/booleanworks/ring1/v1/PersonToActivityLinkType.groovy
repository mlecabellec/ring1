package com.booleanworks.ring1.v1

class PersonToActivityLinkType {

    //static mapWith = "mongo"
    //static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static constraints = {
        defaultLabel(nullable:false)
        labelCode(nullable:false)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true)
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    String defaultLabel
    String labelCode
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }    
    
    public String toString()
    {
        return "PersonToActivityLinkType[" + id + "]"
        //return "PersonToActivityLinkType[" + id + "], " + this.properties
    }
}
