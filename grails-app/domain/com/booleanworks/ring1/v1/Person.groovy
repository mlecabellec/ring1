package com.booleanworks.ring1.v1

class Person {

    //static mapWith = "mongo"
    static searchable = true
    
    static mapping = {
        autoTimestamp true
    }     
    
    static hasMany = [activityLinks:PersonToActivityLink]
    
    static constraints = {
        firstName(nullable:false,blank:false,size:0..128)
        lastName(nullable:false,blank:false,size:0..128)
        fullName(unique:true,nullable:false,blank:false,size:0..128)
        mainEmail(unique:true,nullable:false,blank:false,size:0..128,email:true)
        
        shortDescription(nullable:true,blank:true,size:0..200)
        
        telephoneNumber(nullable:true,blank:true,size:0..32,unique:false)
        mobilePhone(nullable:true,blank:true,size:0..32,unique:false)
        
        cn(nullable:true,blank:true,size:0..128,unique:false)
        dn(nullable:true,blank:true,size:0..128,unique:false)
        directoryUrl(nullable:true,blank:true,size:0..128,unique:false)
        
        mainOrganization(nullable:true)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true) 
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    String firstName
    String lastName
    String fullName
    String mainEmail
    
    String shortDescription
    
    String telephoneNumber
    String mobilePhone
    
    String cn
    String dn
    String directoryUrl
    
    Organization mainOrganization
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }     
    
    public String toString()
    {
        return fullName + " [" + id + "]"
    }
}
