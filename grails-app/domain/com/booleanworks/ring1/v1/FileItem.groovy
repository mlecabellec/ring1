package com.booleanworks.ring1.v1

class FileItem {

    //static mapWith = "mongo"
    static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static hasMany = [chunks : FileChunk]
    
    static constraints = {
        fileName(nullable:false,blank:false,size:0..200) 
        virtualCanonicalPath(nullable:false,blank:false,unique:false,size:0..2000) 
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true) 
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    String fileName 
    String virtualCanonicalPath
    
    Date dateCreated
    Date lastUpdated
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }    
    
    public String toString()
    {
        return "FileItem[" + id + "]"
        //return "FileItem[" + id + "], " + this.properties
    }
}
