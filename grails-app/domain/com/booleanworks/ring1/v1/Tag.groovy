package com.booleanworks.ring1.v1

class Tag {

    //static mapWith = "mongo"
    static searchable = true
    
    static hasMany = [activityLinks : TagToActivityLink, itemLinks : TagToItemLink]
    
    static mapping = {
        autoTimestamp true
    }    
    
    static constraints = {
        
        defaultLabel(nullable:false,unique:true,blank:false,size:0..200)
        labelCode(nullable:false,unique:true,blank:false,size:0..200)
        localeInfo(nullable:true,size:0..64)
        
        parent(nullable:true)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true)
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    String defaultLabel
    String labelCode
    String localeInfo
    
    Tag parent
    
    Date dateCreated
    Date lastUpdated 
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }    
}
