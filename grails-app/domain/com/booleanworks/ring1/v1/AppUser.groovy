package com.booleanworks.ring1.v1

class AppUser {

    //static mapWith = "mongo"
    //static searchable = true
    
    transient springSecurityService

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    
    Person person

    static transients = ['springSecurityService']

    static constraints = {
        username (blank: false, unique: true)
        password (blank: false)
        person(nullable:true)
    }

    static mapping = {
        password column: '`password`'
    }

    Set<AppRole> getAuthorities() {
        AppUserAppRole.findAllByAppUser(this).collect { it.appRole } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }
    
    public String toString()
    {
        
        
        if(person == null)
        {
            return username + " [" + id + "]"
        }else
        {
            return username + " [" + id + "], " + person.toString()
        }
    }    
}
