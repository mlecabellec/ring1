package com.booleanworks.ring1.v1

class Item {

    //static mapWith = "mongo"
    static searchable = true
    
    static mapping = {
        autoTimestamp true
    }    
    
    static constraints = {
        shortDescription(nullable:false,blank:false,size:0..200)
        longDescription(nullable:false,blank:true,size:0..4095)
        
        itemReference(nullable:true,blank:true,size:0..200)
        itemVersion(nullable:true,blank:true,size:0..200)
        itemBatch(nullable:true,blank:true,size:0..200)
        itemSerialNumber(nullable:true,blank:true,size:0..200)        

        isTemplate(nullable:true)
        isPhysicalObject(nullable:true)
        
        dateCreated(nullable:true)
        lastUpdated (nullable:true) 
        invariantUniversalUniqueId(nullable:true)
        variableUniversalUniqueId(nullable:true)        
    }
    
    String shortDescription
    String longDescription
    
    String itemReference
    String itemVersion
    String itemBatch
    String itemSerialNumber
    
    
    Boolean isTemplate
    Boolean isPhysicalObject
    
    Date dateCreated
    Date lastUpdated  
    String invariantUniversalUniqueId
    String variableUniversalUniqueId
    
    def beforeInsert(){
        invariantUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
        variableUniversalUniqueId = invariantUniversalUniqueId
    } 
    
    def beforeUpdate(){
        variableUniversalUniqueId = new UUID(new Date().getTime(), new Random().nextLong()).toString()
    }    
    
    public String toString()
    {
        return "Item[" + id + "]"
        //return "Item[" + id + "], " + this.properties
    }
}
