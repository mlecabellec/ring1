package com.booleanworks.ring1.v1



import grails.test.mixin.*
import spock.lang.*

@TestFor(TagToItemLinkController)
@Mock(TagToItemLink)
class TagToItemLinkControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.tagToItemLinkInstanceList
            model.tagToItemLinkInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.tagToItemLinkInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def tagToItemLink = new TagToItemLink()
            tagToItemLink.validate()
            controller.save(tagToItemLink)

        then:"The create view is rendered again with the correct model"
            model.tagToItemLinkInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            tagToItemLink = new TagToItemLink(params)

            controller.save(tagToItemLink)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/tagToItemLink/show/1'
            controller.flash.message != null
            TagToItemLink.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def tagToItemLink = new TagToItemLink(params)
            controller.show(tagToItemLink)

        then:"A model is populated containing the domain instance"
            model.tagToItemLinkInstance == tagToItemLink
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def tagToItemLink = new TagToItemLink(params)
            controller.edit(tagToItemLink)

        then:"A model is populated containing the domain instance"
            model.tagToItemLinkInstance == tagToItemLink
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/tagToItemLink/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def tagToItemLink = new TagToItemLink()
            tagToItemLink.validate()
            controller.update(tagToItemLink)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.tagToItemLinkInstance == tagToItemLink

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            tagToItemLink = new TagToItemLink(params).save(flush: true)
            controller.update(tagToItemLink)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/tagToItemLink/show/$tagToItemLink.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/tagToItemLink/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def tagToItemLink = new TagToItemLink(params).save(flush: true)

        then:"It exists"
            TagToItemLink.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(tagToItemLink)

        then:"The instance is deleted"
            TagToItemLink.count() == 0
            response.redirectedUrl == '/tagToItemLink/index'
            flash.message != null
    }
}
