package com.booleanworks.ring1.v1



import grails.test.mixin.*
import spock.lang.*

@TestFor(OrgToOrgLinkController)
@Mock(OrgToOrgLink)
class OrgToOrgLinkControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.orgToOrgLinkInstanceList
            model.orgToOrgLinkInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.orgToOrgLinkInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def orgToOrgLink = new OrgToOrgLink()
            orgToOrgLink.validate()
            controller.save(orgToOrgLink)

        then:"The create view is rendered again with the correct model"
            model.orgToOrgLinkInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            orgToOrgLink = new OrgToOrgLink(params)

            controller.save(orgToOrgLink)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/orgToOrgLink/show/1'
            controller.flash.message != null
            OrgToOrgLink.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def orgToOrgLink = new OrgToOrgLink(params)
            controller.show(orgToOrgLink)

        then:"A model is populated containing the domain instance"
            model.orgToOrgLinkInstance == orgToOrgLink
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def orgToOrgLink = new OrgToOrgLink(params)
            controller.edit(orgToOrgLink)

        then:"A model is populated containing the domain instance"
            model.orgToOrgLinkInstance == orgToOrgLink
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/orgToOrgLink/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def orgToOrgLink = new OrgToOrgLink()
            orgToOrgLink.validate()
            controller.update(orgToOrgLink)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.orgToOrgLinkInstance == orgToOrgLink

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            orgToOrgLink = new OrgToOrgLink(params).save(flush: true)
            controller.update(orgToOrgLink)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/orgToOrgLink/show/$orgToOrgLink.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/orgToOrgLink/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def orgToOrgLink = new OrgToOrgLink(params).save(flush: true)

        then:"It exists"
            OrgToOrgLink.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(orgToOrgLink)

        then:"The instance is deleted"
            OrgToOrgLink.count() == 0
            response.redirectedUrl == '/orgToOrgLink/index'
            flash.message != null
    }
}
