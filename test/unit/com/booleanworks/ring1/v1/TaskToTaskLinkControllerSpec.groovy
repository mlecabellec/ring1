package com.booleanworks.ring1.v1



import grails.test.mixin.*
import spock.lang.*

@TestFor(TaskToTaskLinkController)
@Mock(TaskToTaskLink)
class TaskToTaskLinkControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.taskToTaskLinkInstanceList
            model.taskToTaskLinkInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.taskToTaskLinkInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def taskToTaskLink = new TaskToTaskLink()
            taskToTaskLink.validate()
            controller.save(taskToTaskLink)

        then:"The create view is rendered again with the correct model"
            model.taskToTaskLinkInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            taskToTaskLink = new TaskToTaskLink(params)

            controller.save(taskToTaskLink)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/taskToTaskLink/show/1'
            controller.flash.message != null
            TaskToTaskLink.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def taskToTaskLink = new TaskToTaskLink(params)
            controller.show(taskToTaskLink)

        then:"A model is populated containing the domain instance"
            model.taskToTaskLinkInstance == taskToTaskLink
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def taskToTaskLink = new TaskToTaskLink(params)
            controller.edit(taskToTaskLink)

        then:"A model is populated containing the domain instance"
            model.taskToTaskLinkInstance == taskToTaskLink
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/taskToTaskLink/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def taskToTaskLink = new TaskToTaskLink()
            taskToTaskLink.validate()
            controller.update(taskToTaskLink)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.taskToTaskLinkInstance == taskToTaskLink

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            taskToTaskLink = new TaskToTaskLink(params).save(flush: true)
            controller.update(taskToTaskLink)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/taskToTaskLink/show/$taskToTaskLink.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/taskToTaskLink/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def taskToTaskLink = new TaskToTaskLink(params).save(flush: true)

        then:"It exists"
            TaskToTaskLink.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(taskToTaskLink)

        then:"The instance is deleted"
            TaskToTaskLink.count() == 0
            response.redirectedUrl == '/taskToTaskLink/index'
            flash.message != null
    }
}
