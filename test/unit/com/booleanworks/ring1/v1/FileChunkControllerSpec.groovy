package com.booleanworks.ring1.v1



import grails.test.mixin.*
import spock.lang.*

@TestFor(FileChunkController)
@Mock(FileChunk)
class FileChunkControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.fileChunkInstanceList
            model.fileChunkInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.fileChunkInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def fileChunk = new FileChunk()
            fileChunk.validate()
            controller.save(fileChunk)

        then:"The create view is rendered again with the correct model"
            model.fileChunkInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            fileChunk = new FileChunk(params)

            controller.save(fileChunk)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/fileChunk/show/1'
            controller.flash.message != null
            FileChunk.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def fileChunk = new FileChunk(params)
            controller.show(fileChunk)

        then:"A model is populated containing the domain instance"
            model.fileChunkInstance == fileChunk
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def fileChunk = new FileChunk(params)
            controller.edit(fileChunk)

        then:"A model is populated containing the domain instance"
            model.fileChunkInstance == fileChunk
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/fileChunk/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def fileChunk = new FileChunk()
            fileChunk.validate()
            controller.update(fileChunk)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.fileChunkInstance == fileChunk

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            fileChunk = new FileChunk(params).save(flush: true)
            controller.update(fileChunk)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/fileChunk/show/$fileChunk.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/fileChunk/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def fileChunk = new FileChunk(params).save(flush: true)

        then:"It exists"
            FileChunk.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(fileChunk)

        then:"The instance is deleted"
            FileChunk.count() == 0
            response.redirectedUrl == '/fileChunk/index'
            flash.message != null
    }
}
