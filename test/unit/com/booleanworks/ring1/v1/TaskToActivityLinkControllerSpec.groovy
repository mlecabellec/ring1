package com.booleanworks.ring1.v1



import grails.test.mixin.*
import spock.lang.*

@TestFor(TaskToActivityLinkController)
@Mock(TaskToActivityLink)
class TaskToActivityLinkControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.taskToActivityLinkInstanceList
            model.taskToActivityLinkInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.taskToActivityLinkInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def taskToActivityLink = new TaskToActivityLink()
            taskToActivityLink.validate()
            controller.save(taskToActivityLink)

        then:"The create view is rendered again with the correct model"
            model.taskToActivityLinkInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            taskToActivityLink = new TaskToActivityLink(params)

            controller.save(taskToActivityLink)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/taskToActivityLink/show/1'
            controller.flash.message != null
            TaskToActivityLink.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def taskToActivityLink = new TaskToActivityLink(params)
            controller.show(taskToActivityLink)

        then:"A model is populated containing the domain instance"
            model.taskToActivityLinkInstance == taskToActivityLink
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def taskToActivityLink = new TaskToActivityLink(params)
            controller.edit(taskToActivityLink)

        then:"A model is populated containing the domain instance"
            model.taskToActivityLinkInstance == taskToActivityLink
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/taskToActivityLink/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def taskToActivityLink = new TaskToActivityLink()
            taskToActivityLink.validate()
            controller.update(taskToActivityLink)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.taskToActivityLinkInstance == taskToActivityLink

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            taskToActivityLink = new TaskToActivityLink(params).save(flush: true)
            controller.update(taskToActivityLink)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/taskToActivityLink/show/$taskToActivityLink.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/taskToActivityLink/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def taskToActivityLink = new TaskToActivityLink(params).save(flush: true)

        then:"It exists"
            TaskToActivityLink.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(taskToActivityLink)

        then:"The instance is deleted"
            TaskToActivityLink.count() == 0
            response.redirectedUrl == '/taskToActivityLink/index'
            flash.message != null
    }
}
